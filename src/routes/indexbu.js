import React from "react";
import {Layout} from "layouts/LayoutHelper";
import Login from "components/user/auth/login/Login";
import LoginPage from "components/user/auth/page/LoginPage";

const routes = [
  {
    path: '/*',
    component: () => <Layout layoutId="defaultLayout" />,
  },
  {
    path: '/login',
    component: Login,
    exact: true,
  },
  {
    path: '/login-page',
    component: LoginPage,
  },

];

export default routes;
