import {takeEvery} from "redux-saga";
import {call, put, fork} from "redux-saga/effects";
import Configs from 'configs/configuration';
import defaultSiteConfig from './default-site-config';
import {storeSiteConfig, FETCH_SITE_CONFIG} from "layouts/actions";
import Requester from "common/network/http/Request";
import {getTimeEnableForCacheConfig} from 'common';

const LOCALSTORAGE_SITE_CONFIG = 'LOCALSTORAGE_SITE_CONFIG';
const LOCALSTORAGE_SITE_CONFIG_TIME = 'LOCALSTORAGE_SITE_CONFIG_TIME';

const loadSiteConfigFromLocal = () => {
  const isEnableSiteConfigCaching = process.env.REACT_APP_ANABLE_SITE_CONFIG_CACHING
  if(!isEnableSiteConfigCaching || isEnableSiteConfigCaching !== 'true') {
    return;
  }
  let time = localStorage.getItem(LOCALSTORAGE_SITE_CONFIG_TIME);
  if (!time) {
    return;
  }
  time = parseInt(time);
  if (!time || time <= new Date().getTime()) {
    return;
  }
  let siteConfig = localStorage.getItem(LOCALSTORAGE_SITE_CONFIG);
  if (!siteConfig) {
    return;
  }
  return JSON.parse(siteConfig);
}

const storeSiteConfigToLocal = (siteConfig) => {
  if (!siteConfig || (siteConfig && Object.keys(siteConfig).length <= 0)) {
    return;
  }
  localStorage.setItem(LOCALSTORAGE_SITE_CONFIG, JSON.stringify(siteConfig));
  localStorage.setItem(LOCALSTORAGE_SITE_CONFIG_TIME, new Date().getTime() + getTimeEnableForCacheConfig());
}

function* fetchSiteConfigs(action) {
  const {onSuccess, onFail} = action;
  const configsFromLocal = loadSiteConfigFromLocal();
  if (configsFromLocal) {
    yield put(storeSiteConfig(configsFromLocal));
    return;
  }

  let site = window.location.hostname;

  const response = yield call(
    Requester.get,
    Configs.apiUrl.siteConfigs(site),
  );
  const result = response;
  if (result) {
    yield put(storeSiteConfig(result));
    if (onSuccess) {
      onSuccess(result);
    }
    storeSiteConfigToLocal(result);
    return;
  }

  yield put(storeSiteConfig(defaultSiteConfig));
}


export const fetchSiteConfigsAction = function* fetchSiteConfigsSaga() {
  yield* takeEvery(FETCH_SITE_CONFIG, fetchSiteConfigs);
};

export default [
  fork(fetchSiteConfigsAction),
];
