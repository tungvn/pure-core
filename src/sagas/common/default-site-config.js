export default {
  agency_name: process.env.REACT_APP_CLIENT_NAME,
  color_scheme: "#eb7374",
  login_background: "/media/images/login-bg.jpg",
  favicon: "/favicon.png"
}