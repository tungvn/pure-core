import oauth2 from "components/user/auth/oauth2/sagas";
import commonSaga from './common'

export default function* root() {
  try {
    yield oauth2;
    yield commonSaga;
  } catch (e) {
    console.log(e);
  }
}
