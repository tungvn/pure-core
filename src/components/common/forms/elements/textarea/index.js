import React from "react";
import {sheet, DEFAULT_COLOR} from 'common/css/generateCss';
import "./stylesheet.css";

class PixelzTextArea extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    const { input } = this.props;
    const { value } = this.state;
    if (input) {
      input.onChange(event, event.target.value, value);
    }
    this.setState({ value: event.target.value });
  }


  render() {
    const props = { ...this.props };
    let { className, label, height, plan, required, color } = props;
    const { input, custom } = props;
    delete props.input;
    delete props.meta;
    delete props.disabled;
    color = color || DEFAULT_COLOR;
    sheet('ptextarea').innerHTML = ` .ui-input input:focus, .ui-input textarea:focus, 
    .ui-input select:-webkit-autofill:focus  { border: 1px solid ${color} !important; }`
    ;
    const { activeClass } = this.state;
    className = className || '';
    className = `${className} ui-input ${activeClass}`;
    return (
      <div className={className}>
        <textarea
          disabled={this.props.disabled}
          onChange={this.handleChange}
          {...props}
          {...input}
          {...custom}
        />

      </div>
    );
  }
}

export default PixelzTextArea;
