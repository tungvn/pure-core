import React from "react";
import {sheet, DEFAULT_COLOR} from 'common/css/generateCss';
import "./stylesheet.css";

class PixelzInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.onBlur = this.onBlur.bind(this);
    this.onFocus = this.onFocus.bind(this);
  }

  onFocus() {
    const { type } = this.props;
    this.setState({ activeClass: 'on-focus' });
    if (type === 'textarea') {
      this.refs.inputArea.focus();
    } else {
      this.refs.inputText.focus();
    }
  }

  onBlur() {
    const { value } = this.state;
    let activeClass = '';
    if (value) {
      activeClass = 'active-data';
    }
    this.setState({ activeClass });
  }

  handleChange = (event) => {
    const { input } = this.props;
    const { value } = this.state;

    if (input) {
      input.onChange(event.target.value, value);
    }
    this.setState({ value: event.target.value });
  }

  render() {
    const props = { ...this.props };
    let { className, label, type, height, plan, required, color } = props;
    const { input, custom } = props;
    delete props.input;
    delete props.meta;
    delete props.custom;
    delete props.disable;
    delete props.disabled;
    color = color || DEFAULT_COLOR;
    sheet('pinput').innerHTML = `.ui-pinput label i { color: ${color} !important; }`;


    const { activeClass, value } = this.state;
    className = className || '';
    className = `${className} ui-pinput ${activeClass}`;
    type = type || 'input';
    const style = height ? { height } : {};
    return (
      <div className={className} onClick={this.onFocus}>
        <label className="clearfix no-selectable">
          {label}
          {
            required && <i className="pull-right">*</i>
          }
        </label>
        {
          type && type !== 'textarea' &&
          <input
            onChange={this.handleChange}
            ref="inputText"
            onFocus={this.onFocus}
            onBlur={this.onBlur}
            {...input}
            {...custom}
            {...props}
          />
        }
        {
          type && type === 'textarea' &&
          <textarea
            style={style}
            onChange={this.handleChange}
            ref="inputArea"
            onFocus={this.onFocus}
            onBlur={this.onBlur}
            disabled={this.props.disabled}
            {...input}
            {...custom}
            {...props}
          />
        }
      </div>
    );
  }
}

export default PixelzInput;
