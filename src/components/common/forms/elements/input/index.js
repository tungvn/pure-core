import React from "react";
import "./stylesheet.css";

class PixelzInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  handleChange = (event) => {
    const { input, custom } = this.props;
    const { value } = this.state;

    if (input) {
      input.onChange(event.target.value, value);
    }
    this.setState({ value: event.target.value });
  }

  render() {
    const props = { ...this.props };
    let { className } = props;
    const { input, meta,disable, custom  } = props;
    delete props.input;
    delete props.meta;
    delete props.onFocus;
    delete props.onBlur;
    delete props.disable;

    const { activeClass } = this.state;
    className = className || '';
    className = `${className} ui-input ${activeClass}`;
    return (
      <div className={className}>
        <input
          onChange={this.handleChange}
          {...input}
          {...custom}
          {...props}
        />

      </div>
    );
  }
}

export default PixelzInput;
