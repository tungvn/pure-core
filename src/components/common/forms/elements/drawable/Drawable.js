import React from 'react';
import Loading from "components/common/views/loading";
import {connect} from "react-redux";
import RejectionInformation from 'components/common/views/img-status/rejection-icon';
import Configs from 'configs/configuration';
import './stylesheet.css';

/**
 * Created by Peter Hoang Nguyen
 * Email: vntopmas@gmail.com
 * Tel: 0966298666
 * created date 03/09/2017
 **/
class DrawText extends React.Component {
  constructor(props) {
    super(props);
    this.state = {style: {display: "none"}, loading: true, canvasDimension: {}};
  }

  componentDidMount() {
    const {id, siteConfigs} = this.props;
    var canvas = new window.fabric.Canvas(id, {
      isDrawingMode: true
    });
    canvas.freeDrawingBrush.width = 6;
    // canvas.freeDrawingBrush.color = 'red';
    canvas.freeDrawingBrush.color = siteConfigs.color_scheme || 'red';
    canvas.on('mouse:up', this.handleChange);
    this.initBackgroundAndCanvasSize(canvas);
    this.setState({canvas});
  }

  handleChange = (event) => {
    const {id, input, onCanvasChange, width} = this.props;
    const {value, canvas} = this.state;
    let newImage = canvas.toDataURL({
      format: 'jpg',
      multiplier: 2
    });
    let newValue;
    try {
      newValue = canvas.toJSON();
    } catch (err) {

    }
    const thumbSize = this.getThumbSize();
    if (input) {
      input.onChange(newValue, value);
    }
    this.resizeImage(newImage, thumbSize.width, thumbSize.height, (base64) => {
      if (onCanvasChange) {
        onCanvasChange(id, base64);
      }
      if (this.refs.drawableComponent) {
        this.setState({value: newValue, newImage: base64});
      }
    });
  }

  resizeImage = (base64, w, h, callback) => {
    const img = new Image();
    const canvas = document.createElement('canvas');
    const cxt = canvas.getContext('2d');
    img.onload = () => {
      canvas.width = w;
      canvas.height = h;
      cxt.drawImage(img, 0, 0, w, h);
      callback(canvas.toDataURL('image/jpeg'));
      // document.body.removeChild(canvas);
    };
    img.src = base64;
  }

  componentWillReceiveProps(nextProps) {
    const {canvas} = this.state;
    if (!canvas) {//||  this.props.width == nextProps.width
      return;
    }
    if (nextProps.width !== this.props.width) {
      this.initBackgroundAndCanvasSize(canvas, nextProps);
      const oldWidth = this.props.width;
      const newWidth = nextProps.width;
      this.zooming(Math.abs(newWidth / oldWidth));
    }
    canvas.isDrawingMode = nextProps.drawable;

    if (!nextProps.drawable) {
      canvas.selection = false;
      canvas.forEachObject(function (o) {
        o.selectable = false;
      });
    }

    if (this.props.clearMarkingsTimeAt !== nextProps.clearMarkingsTimeAt) {
      this.clearMarking();
    }

  }

  initBackgroundAndCanvasSize = (canvas, nextProps) => {
    if (!canvas) {
      return;
    }

    const {width, bgSrc, height, imgName} = nextProps || this.props;
    window.fabric.Image.fromURL(bgSrc, (oImg) => {
      const imgWidth = oImg.width;
      const imgHeight = oImg.height;
      const canvasDimension = this.getNewHeightSize(imgWidth, imgHeight, nextProps);
      canvas.setBackgroundImage(oImg);
      canvas.backgroundImage.scaleToWidth(canvasDimension.width);
      canvas.backgroundImage.scaleToHeight(canvasDimension.height);

      canvas.setDimensions(canvasDimension);
      canvas.renderAll();
      if (this.refs.drawableComponent) {
        let drawTimeout;

        if (this.state.loading) {
          clearTimeout(this.state.drawTimeout);
          drawTimeout = setTimeout(() => {
            this.initBackgroundAndCanvasSize(canvas);
          }, 10);
        }
        this.setState({
          imgWidth, imgHeight,
          loading: false,
          drawTimeout: drawTimeout,
          backgroundSize: {
            width: oImg.width,
            height: oImg.height
          },
          dimension: canvasDimension, style: {display: "inline-block"}
        });

      }


    }, {crossOrigin: 'Anonymous'});
  }

  zooming = (scale) => {
    const {canvas} = this.state;
    var objects = canvas.getObjects();
    for (var i in objects) {
      var scaleX = objects[i].scaleX;
      var scaleY = objects[i].scaleY;
      var left = objects[i].left;
      var top = objects[i].top;

      var tempScaleX = scaleX * scale;
      var tempScaleY = scaleY * scale;
      var tempLeft = left * scale;
      var tempTop = top * scale;

      objects[i].scaleX = tempScaleX;
      objects[i].scaleY = tempScaleY;
      objects[i].left = tempLeft;
      objects[i].top = tempTop;

      objects[i].setCoords();
    }

    canvas.renderAll();
  }

  clearMarking = () => {
    const {canvas} = this.state;
    const {onClearMarking} = this.props;
    canvas.clear();
    this.initBackgroundAndCanvasSize(canvas);
    if (onClearMarking) {
      onClearMarking();
    }
  }

  getNewHeightSize = (bgWidth, bgHeight, nextProps) => {
    const {width, height} = nextProps || this.props;
    let rateWidth = width / bgWidth;
    let rateHeight = height / bgHeight;
    let rate = rateWidth > rateHeight ? rateHeight : rateWidth;
    return {
      height: Math.abs(bgHeight * rate),
      width: Math.abs(bgWidth * rate)
    };
  }

  getThumbSize = () => {
    const defaultSize = 800;
    const {backgroundSize} = this.state || {width: defaultSize, height: defaultSize};
    const {width, height} = backgroundSize;
    if (width <= defaultSize && height <= defaultSize) {
      return {...backgroundSize}
    }
    let newWidth;
    let newHeigh;
    if (width > height) {
      newWidth = defaultSize;
      newHeigh = Math.abs(backgroundSize.height * (newWidth / width));
    } else {
      newHeigh = defaultSize;
      newWidth = Math.abs(backgroundSize.width * (newHeigh / height));
    }

    return {
      width: newWidth,
      height: newHeigh
    }
  }

  getCanvasWidth = () => {
    const {width, height} = this.props;
    return width > height ? height : width;
  }

  render() {
    const {id, width, height, status, configColor} = this.props;
    let {style, loading, canvasDimension} = this.state;
    return (
      <div className={`drawable-component ${loading ? 'loading-panel' : ''}`}
           style={{width: `${width}px`, height: `${height}px`}}
           ref="drawableComponent">
        <div style={style}>
          <canvas id={id} width={canvasDimension.width} onClick={this.addNode}/>
        </div>

        {
          status && status.id === Configs.rejected_by_client_status.id &&
          <RejectionInformation/>
        }

        {
          loading &&
          <span className="loading-subpanel">
          <Loading className="loading"/>
        </span>
        }

      </div>
    )
      ;
  }
}

const mapStateToProps = (state, props) => {
  return {
    siteConfigs: state.common.siteConfigs || {},
  };
};

export default connect(mapStateToProps)(DrawText);
