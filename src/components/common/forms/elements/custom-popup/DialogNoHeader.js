/**
 * Created by Peter Hoang Nguyen on 4/3/2017.
 */
/**
 * Created by Peter Hoang Nguyen on 4/1/2017.
 */
import React from 'react';
import { multiLanguage, t1 } from 'i18n';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import './stylesheet.css';

/**
 * Created by Peter Hoang Nguyen
 * Email: vntopmas@gmail.com
 * Tel: 0966298666
 * created date 01/04/2017
 **/
class DialogNoHeader extends React.Component {

  render() {
    const { intl, closeOn } = this.props;
    let { bodyClassName } = this.props;
    const close = t1(intl, 'close');
    if (bodyClassName) {
      bodyClassName += ' dialog-content-padding0 no-header-dialog';
    } else {
      bodyClassName = 'dialog-content-padding0 no-header-dialog';
    }

    return (
      <Dialog
        {...this.props}
        onRequestClose={closeOn}
        bodyClassName={bodyClassName}
      >
        <FlatButton
          className="close-popup" onClick={closeOn} alt={close}
        >
          <i className="mi mi-close" aria-hidden="true" />
        </FlatButton>
        {
          this.props.children
        }
      </Dialog>
    );
  }
}

export default multiLanguage(DialogNoHeader);
