import React from "react";
import {Field} from "redux-form";
import myPixelzInput from "components/common/forms/elements/pinput";
import myInput from "components/common/forms/elements/input";
import myTags from "components/common/forms/elements/tags";
import myTextarea from "components/common/forms/elements/textarea";
import myDrawable from "components/common/forms/elements/drawable/Drawable";
import PixelzCheckBox from "components/common/forms/elements/pcheckbox";
import MenuItem from "material-ui/MenuItem";
import {
  Checkbox as MUCheckBox,
  RadioButtonGroup as MURadioButtonGroup,
  SelectField as MUSelectField,
  TextField as Text,
  Toggle as MUToggle,
  DatePicker as MUDatePicker,
  AutoComplete as MUAutoComplete,
  Slider as MUSlider,
  TimePicker as MUTimePicker
} from "redux-form-material-ui";

export const PCheckBox = (props) => <Field component={PixelzCheckBox} {...props} />;
export const Checkbox = (props) => <Field component={MUCheckBox} {...props} />;
export const RadioButtonGroup = (props) => <Field component={MURadioButtonGroup} {...props} />;
export const SelectField = (props) => <Field component={MUSelectField} {...props} />;
export const TextField = (props) => <Field component={Text} {...props} />;
export const Toggle = (props) => <Field component={MUToggle} {...props} />;
export const DatePicker = (props) => <Field component={MUDatePicker} {...props} />;
export const AutoComplete = (props) => <Field component={MUAutoComplete} {...props} />;
export const Slider = (props) => <Field component={MUSlider} {...props} />;
export const TimePicker = (props) => <Field component={MUTimePicker} {...props} />;
export const PixelInput = (props) => <Field component={myPixelzInput} {...props} />;
export const Input = (props) => <Field component={myInput} {...props} />;
export const Tags = (props) => <Field component={myTags} {...props} />;
export const Textarea = (props) => <Field component={myTextarea} {...props} />;
export const Drawable = (props) => <Field component={myDrawable} {...props} />;


export class Element extends React.Component {
  render() {
    const { schema } = this.props;

    switch (schema.type) {
      case 'text': {
        return TextField(schema);
      }
      case 'checkbox': {
        return Checkbox(schema);
      }
      case 'select': {
        const { options, ...passedSchema } = schema;
        passedSchema.children =
          Object
            .keys(options)
            .map((value) => <MenuItem key={value} value={value} primaryText={options[value]} />);

        return SelectField(passedSchema);
      }
      default:
        return <div>Element {schema.type} not defined</div>;
    }
  }
}

Element.propTypes = {
  schema: React.PropTypes.object.required,
};
