import React from "react";
import {sheet, DEFAULT_COLOR} from 'common/css/generateCss';
import "./stylesheet.css";

/**
 * Created by Ha Viet Duc
 * created date 22/04/2017
 **/
class MyCheckBox extends React.Component {

  constructor(props) {
    super(props);
    this.state = {value: false};
    this.onChange = this.onChange.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const { input } = nextProps;
    if(input.value !== this.state.checked) {
      this.setState({value: input.value});
    }
  }

  onChange(event) {
    const { onChanged, input, onChange, onDataChange } = this.props;
    const value = !this.state.value;

    if (input) {
      input.onChange(value);
    }
    if (onChange) {
      onChange(value, event);
    }
    if (onChanged) {
      onChanged(value, event);
    }
    this.setState({value});
  }


  render() {
    let { className, label, lclassName, defaultValue, input, color } = this.props;
    const props = {...this.props};
    delete props.lclassName;
    delete props.defaultValue;
    delete props.input;
    delete props.meta;
    delete props.onChanged;

    let { selected } = this.state;
    color = color || DEFAULT_COLOR;
    sheet('pcheckbox').innerHTML = `.ui-pcheckbox label.isSelected:before { border: 8.5px solid ${color} !important; }`;
    className = className ? `${className} ui-pcheckbox` : 'ui-pcheckbox';
    const lclass = input.value ? `${lclassName} isSelected no-selectable` : `${lclassName} no-selectable`;
    return (
      <div className={className} >
        <input type="checkbox" ref="checkbox" {...props} onClick={this.onChange} />
        <label className={lclass} onClick={this.onChange} >{label}</label>
      </div>
    );
  }
}

export default MyCheckBox;
