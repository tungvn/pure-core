import React from "react";
import "./stylesheet.css";

class BigSearchInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.handleChange = this.handleChange.bind(this);
  }
  handleChange(event) {
    const { input, onInputChange } = this.props;
    const { value } = this.state;

    if (input) {
      input.onChange(event, event.target.value, value);
    }

    if (onInputChange) {
      onInputChange(event, event.target.value, value);
    }
    this.setState({ value: event.target.value });
  }

  render() {
    const props = { ...this.props };
    delete props.input;
    delete props.meta;
    delete props.onInputChange;
    return (
      <div className="big-ui-pinput" onClick={this.onFocus}>

        <input
          onChange={this.handleChange}
          ref="inputText"
          {...props}
        />
      </div>
    );
  }
}

export default BigSearchInput;
