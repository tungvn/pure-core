import React from "react";
import Draggable from 'react-draggable';
import "./stylesheet.css";
import Loading from "../loading";
import ImgStatus from '../img-status';
import Configs from 'configs/configuration';

/**
 * Created by Ha Viet Duc
 * created date 22/04/2017
 **/
class ViewImage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      activeDrags: 0,
      deltaPosition: {
        x: 0, y: 0
      },
      controlledPosition: {
        x: -400, y: 200
      }
    };
    this.handleImageLoaded = this.handleImageLoaded.bind(this);
    this.handleLoadImageError = this.handleLoadImageError.bind(this);
  }


  handleDrag = (e, ui) => {
    const {x, y} = this.state.deltaPosition;
    this.setState({
      deltaPosition: {
        x: x + ui.deltaX,
        y: y + ui.deltaY,
      }
    });
  }

  onStart = () => {
    this.setState({
      activeDrags: true,
      deltaPosition: {
        x: 0,
        y: 0,
      }
    });
  }

  onStop = () => {
    this.setState({
      activeDrags: false,
      deltaPosition: {
        x: 0,
        y: 0,
      }
    });
  }

  // For controlled component
  adjustXPos = (e) => {
    e.preventDefault();
    e.stopPropagation();
    const {x, y} = this.state.controlledPosition;
    this.setState({controlledPosition: {x: x - 10, y}});
  }

  adjustYPos = (e) => {
    e.preventDefault();
    e.stopPropagation();
    const {controlledPosition} = this.state;
    const {x, y} = controlledPosition;
    this.setState({controlledPosition: {x, y: y - 10}});
  }

  handleImageLoaded = (event) => {
    this.setState({loading: false});
  }

  handleLoadImageError = (event) => {
    this.setState({loading: true});
  }

  render() {
    const {loading} = this.state;
    const {alt, src, width, height, onContextMenu, status, onClick} = this.props;
    let {className, borderColor} = this.props;
    borderColor = borderColor || '#eb7374';
    let style = {width: '100%', height: `${height}px`};
    if (className === 'highlight') {
      style = {...style, boxShadow: `0 0 0 3px ${borderColor}`}
    } else if (className === 'small-highlight') {
      style = {...style, boxShadow: `0 0 0 1.8px ${borderColor}`}
    }
    className = className ? `${className} ui-view-image` : 'ui-view-image';
    const displayStyle = {fontSize: `${height}px`};

    const dragHandlers = {onStart: this.onStart, onStop: this.onStop};

    return (
      <div onClick={onClick} onContextMenu={onContextMenu}>
        <div style={{height: 0, overflow: 'visible'}}>
          <div className={`${className}`}
               style={{...style, display: `${this.state.activeDrags ? 'flex' : 'none'}`}}>
            <ImgStatus status={status}/>
            <img draggable={false}
                 style={loading ? {display: 'none'} : displayStyle}
                 alt={alt}
                 src={src}
                 onError={this.handleLoadImageError}
                 onLoad={this.handleImageLoaded}
            />

            { loading &&
            <Loading />
            }
          </div>
        </div>
        <Draggable
          style={{zIndex: `${this.state.activeDrags ? 1000: 1}`}}
          position={{...this.state.deltaPosition}}
                   {...dragHandlers}>
          <div style={{...style, zIndex: `${this.state.activeDrags ? 1000: 1}`}} className={`${className}`}>

            <ImgStatus status={status}/>
            <img draggable={false}
                 style={loading ? {display: 'none'} : displayStyle}
                 alt={alt}
                 src={src}
                 onError={this.handleLoadImageError}
                 onLoad={this.handleImageLoaded}
            />

            { loading &&
            <Loading />
            }

          </div>
        </Draggable>

      </div>
    );
  }
}

export default ViewImage;
