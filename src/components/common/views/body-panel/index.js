import React from "react";
import "./stylesheet.css";

class Panel extends React.Component {

  render() {
    const { title, children } = this.props;
    let { className } = this.props;
    className = className || '';
    className = `${className} ui-body-panel`;
    return (
      <div className={className}>
        <div className="header">
          {title}
        </div>
        <div className="content">
          {children}
        </div>

      </div>
    );
  }
}

export default Panel;
