import React from "react";
import "./stylesheet.css";

class Panel extends React.Component {

  render() {
    const { title, children } = this.props;
    let { className } = this.props;
    className = className || '';
    className = `${className} ui-panel-p`;
    return (
      <div className={className}>
        <div className="header">
          {title}
        </div>
        {children}
      </div>
    );
  }
}

export default Panel;
