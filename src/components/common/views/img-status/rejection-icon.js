import React from "react";
import "./stylesheet.css";
import {DEFAULT_COLOR} from 'common/css/generateCss';
import SvgIcon, {rejectionIcon} from "common/icons/pixelz-svg";
import {connect} from "react-redux";
import {multiLanguage, t1} from "i18n";

/**
 * Created by Ha Viet Duc
 * created date 22/04/2017
 **/
class RejectionIcon extends React.Component {
  constructor(props) {
    super(props);
    this.state = {loading: true};

  }

  render() {
    let {siteConfigs, className, intl} = this.props;
    let color = siteConfigs.color_scheme || DEFAULT_COLOR;
    className = `${className} reject-sign`;
    return (
      <div className={className} style={{backgroundColor: color}} title={t1(intl, 'image_has_been_rejected')}>
          <span className="no-selectable">
            <p className="reject-icon">
               <SvgIcon path={rejectionIcon} className="is-icon-add-1 marking-icon"/>
            </p>
          </span>
      </div>
    )
  }
}

const mapStateToProps = (state, props) => {
  return {
    siteConfigs: state.common.siteConfigs || {},
  };
};

export default connect(mapStateToProps)(multiLanguage(RejectionIcon));
