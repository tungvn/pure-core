import React from "react";
import "./stylesheet.css";
import {DEFAULT_COLOR} from 'common/css/generateCss';
import SvgIcon, {iconScrible, rejectionIcon} from "common/icons/pixelz-svg";
import {connect} from "react-redux";
import {multiLanguage, t1} from "i18n";
import RejectionInformation from 'components/common/views/img-status/rejection-icon';
import Configs from 'configs/configuration';

/**
 * Created by Ha Viet Duc
 * created date 22/04/2017
 **/
class ImgStatus extends React.Component {
  constructor(props) {
    super(props);
    this.state = {loading: true};
    this.handleImageLoaded = this.handleImageLoaded.bind(this);
    this.handleLoadImageError = this.handleLoadImageError.bind(this);
  }

  handleImageLoaded(event) {
    this.setState({loading: false});
  }

  handleLoadImageError(event) {
    this.setState({loading: true});
  }

  render() {
    const {status, intl} = this.props;
    let {siteConfigs} = this.props;
    let color = siteConfigs.color_scheme || DEFAULT_COLOR;
    if (status && status.id === Configs.rejected_by_client_status.id) {
      return (
        <RejectionInformation/>
      )
    }
    if (status && status.id === Configs.rejection_marked_status.id) {
      return (
        <div className="reject-sign" style={{backgroundColor: color}} title={t1(intl, 'image_has_been_marked')}>
          <span className="no-selectable">
            <p>
               <SvgIcon path={iconScrible} className="is-icon-add-1 marking-icon"/>
            </p>
          </span>
        </div>
      )
    }

    return (
      <span style={{display: 'none'}}>
      </span>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    siteConfigs: state.common.siteConfigs || {},
  };
};

export default connect(mapStateToProps)(multiLanguage(ImgStatus));
