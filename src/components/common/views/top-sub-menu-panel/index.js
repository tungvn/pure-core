import React from "react";
import "./stylesheet.css";
import DownloadButton from "../../control-common/Download";
import ShareButton from "../../control-common/Share";
import SvgIcon, {arrowMoveLeft, pictureIcon} from "common/icons/pixelz-svg";
import Slider from "material-ui/Slider";
import {setShowQuickViewStatus} from "../../quickview/actions";
import {connect} from "react-redux";
import {MuiThemeProvider} from "material-ui/styles";

class TopControl extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.onSlideImage = this.onSlideImage.bind(this);
    this.onViewImageBigger = this.onViewImageBigger.bind(this);
    this.onViewImageSmaller = this.onViewImageSmaller.bind(this);
    this.handleShowQuickView = this.handleShowQuickView.bind(this);
  }

  componentDidMount() {
    let { defaultSlideValue, onSizeSlide } = this.props;
    defaultSlideValue = defaultSlideValue || 0.12;
    onSizeSlide(undefined, defaultSlideValue);
    this.setState({ slideSize: defaultSlideValue });
  }

  onSlideImage(event, value) {
    const { onSizeSlide } = this.props;
    if (value <= 0) {
      value = 0.01;
    }
    onSizeSlide(event, value);
    this.setState({ slideSize: value });
  }

  onViewImageSmaller(event) {
    const { slideSize } = this.state;
    const { onSizeSlide } = this.props;
    let newSlideSize = slideSize - 0.1;
    if (newSlideSize <= 0) {
      newSlideSize = 0.01;
    }

    onSizeSlide(event, newSlideSize);
    this.setState({ slideSize: newSlideSize });
  }

  onViewImageBigger(event) {
    const { slideSize } = this.state;
    const { onSizeSlide } = this.props;
    let newSlideSize = slideSize + 0.1;
    if (newSlideSize > 1) {
      newSlideSize = 1;
    }
    onSizeSlide(event, newSlideSize);
    this.setState({ slideSize: newSlideSize });
  }

  handleShowQuickView() {
    const { dispatch } = this.props;
    dispatch(setShowQuickViewStatus(true));
  }

  render() {
    const { slideSize } = this.state;

    return (
      <div className="ui-top-control clearfix">
        <div className="pull-left">
          <ul className="control-list">
            <li>
              <span className="collection-name">Spring 2015</span>
            </li>
            <li>
              <DownloadButton />
            </li>
            <li>
              <ShareButton />
            </li>
          </ul>
        </div>

        <div className="pull-right">
          <ul className="control-list">
            <li onClick={this.onViewImageSmaller}>
              <SvgIcon path={pictureIcon} className="color-white  is-icon-picture m-l-10" />
            </li>
            <li>

              <MuiThemeProvider muiTheme={muiTheme}>
                <Slider className="slider" onChange={this.onSlideImage} value={slideSize} defaultValue={slideSize} />
              </MuiThemeProvider>
            </li>
            <li onClick={this.onViewImageBigger}>
              <SvgIcon path={pictureIcon} className="color-white big-more-picture is-icon-picture m-r-10" />
            </li>
            <li>
              <a href="#" className="move-left-control-link" onClick={this.handleShowQuickView}>
                <SvgIcon path={arrowMoveLeft} className="is-icon-arrow-move-left m-l-10" />
              </a>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}

export default connect()(TopControl);
