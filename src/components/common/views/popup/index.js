import React from "react";
import ReactDOM from "react-dom";
import {connect} from "react-redux";
import {PixelInput} from "components/common/forms/elements";
import "./stylesheet.css";

class Popup extends React.Component {
  state = {}

  // componentWillReceiveProps(nextProps) {
  //   const {tabNumber, showQuickView} = nextProps
  //   if(this.props.open !== nextProps.open) {
  //     this.setState({tabNumber, showQuickView})
  //   }
  // }

  componentDidMount() {
    window.rootAppContainer.addEventListener('click', this.handleClickOutside);
    this.setState({ mounted: true });

  }

  componentWillUnmount() {
    this.setState({ mounted: false });
    window.rootAppContainer.removeEventListener('click', this.handleClickOutside);
  }


  handleClickOutside = (evt) => {
    if (this.state.mounted) {
      const { onClose, dispatch, showDocumentDetailStatus } = this.props;
      const area = ReactDOM.findDOMNode(this.refs.popup);
      if (!showDocumentDetailStatus && !area.contains(evt.target) && onClose) {
        onClose();
      }
    }
  }

  render() {
    const { title, children, footer, open, onClose } = this.props;

    let dialogStyle = {
      // transition: 'transform 400ms cubic-bezier(0.05, 0.74, 0.27, 0.99)',
      transform: 'translate3d(0, -10000px, 0)',

    };
    if (open) {
      dialogStyle = {
        // transition: 'transform 400ms cubic-bezier(0.05, 0.74, 0.27, 0.99)',
        transform: 'translate3d(0, 0, 0)',
      };
    }

    return (
      <div className="my-modal" style={dialogStyle}>
        <div className="dialog-modal" style={dialogStyle}>
          <div className="dialog-content">
            <div className="body" ref="popup">
              <span className="close-popup" onClick={onClose}>
                <i className="mi mi-close" aria-hidden="true" />
              </span>
              <div className="ui-header">
                <span>
                  {title}
                </span>
              </div>

              <div className="ui-content">
                {children}
              </div>
              <div className="modal-footer">
                {footer}
              </div>
            </div>
          </div>

        </div>
        <div className="overlay" style={{ display: open ? 'block' : 'none' }} />
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
  }
}
export default connect(mapStateToProps)(Popup);

