/**
 * Created by Peter Hoang Nguyen on 4/14/2017.
 */
import React from "react";
import "./stylesheet.css";
import Configs from 'configs/configuration';
// import {SelectField} from 'components/common/forms/elements';
import FullPaginationType from "./Full";
import ShortestPaginationType from "./Shorter";

/**
 * Created by Peter Hoang Nguyen
 * Email: vntopmas@gmail.com
 * Tel: 0966298666
 * created date 14/04/2017
 **/
class Pagination extends React.Component {
  constructor(props) {
    super(props);
    this.state = { currentPageNumber: 1, startAtPage: 1, value: 1 };
    this.onPageChanged = this.onPageChanged.bind(this);
    this.generateControlOfPageNumbers = this.generateControlOfPageNumbers.bind(this);
    this.calculatePageTotal = this.calculatePageTotal.bind(this);
    this.onBackPage = this.onBackPage.bind(this);
    this.onNextPage = this.onNextPage.bind(this);
    this.onGoFirstPage = this.onGoFirstPage.bind(this);
    this.onGoLastPage = this.onGoLastPage.bind(this);
    this.getTotalPagerControl = this.getTotalPagerControl.bind(this);
    this.handlePageInputOnKeyUp = this.handlePageInputOnKeyUp.bind(this);
    this.handlePageInputOnChange = this.handlePageInputOnChange.bind(this);
    this.onItemPerPageChange = this.onItemPerPageChange.bind(this);
  }


  componentWillReceiveProps(nextProps) {
    let { perPage, total } = this.props;
    if(nextProps.perPage !== perPage || nextProps.total !== total) {
      let pageNumber = this.state.currentPageNumber;
      if(!pageNumber) {
        pageNumber = 1;
      }
      this.onPageChanged(pageNumber);
    } else {
      this.calculatePageTotal();
    }
  }

  componentDidMount() {
    this.calculatePageTotal();
    this.generateControlOfPageNumbers();
  }

  getTotalPagerControl() {
    const { showingPagesControlNumber } = this.props;
    return showingPagesControlNumber || 5;
  }

  generateControlOfPageNumbers(newCurrentPageNumber, startFrom, endAt, itemPerPageChanged) {
    let { total, itemPerPage, onPageChange, iPerPage } = this.props;
    let { currentPageNumber, showingPageToPage, showingPageFromPage, startAtPage} = this.state;
    let showingPagesControlNumber = this.getTotalPagerControl();
    const showingPagesControlNumberDefault = showingPagesControlNumber;
    total = total;
    // iPerPage = iPerPage; // || this.state.iPerPage || Configs.collectionDefaultPaging.limit
    showingPageToPage = endAt || showingPageToPage;
    showingPageFromPage = startFrom || showingPageFromPage;
    let totalPages = this.calculatePageTotal();
    if (itemPerPageChanged === -1) {
      // totalPages = 1;
      iPerPage = -1;
    } else {
      iPerPage = itemPerPageChanged || iPerPage || itemPerPage;
      iPerPage = iPerPage || 10;
      // totalPages = Math.ceil(total / iPerPage);
      if (showingPageToPage < showingPagesControlNumberDefault && showingPageToPage < totalPages) {
        showingPageToPage = (totalPages > showingPagesControlNumberDefault) ? showingPagesControlNumberDefault : total;
      }
    }
    if (newCurrentPageNumber) {
      currentPageNumber = newCurrentPageNumber;
    }
    currentPageNumber = currentPageNumber || startAtPage;

    showingPageToPage = showingPageToPage || showingPagesControlNumber;
    showingPageFromPage = showingPageFromPage || startAtPage;
    showingPagesControlNumber--;
    if (currentPageNumber === showingPageToPage) {

      showingPageToPage = showingPageToPage + currentPageNumber - startAtPage;
    } else if (currentPageNumber === showingPageFromPage && showingPageFromPage !== startAtPage) {
      showingPageToPage = showingPageFromPage;
    }

    if (showingPageToPage > totalPages) {
      showingPageToPage = totalPages;
    }

    showingPageFromPage = showingPageToPage - showingPagesControlNumber;

    if (showingPageFromPage < startAtPage) {
      showingPageFromPage = startAtPage;
    }
    if (showingPageFromPage === startAtPage && totalPages >= showingPagesControlNumberDefault) {
      showingPageToPage = showingPagesControlNumberDefault;
    }
    if (currentPageNumber > showingPageToPage && currentPageNumber > totalPages) {
      currentPageNumber = showingPageToPage;
    } else {
      showingPageToPage = currentPageNumber;
    }
    // TODO: using sagas
    if (onPageChange) {
      if (iPerPage === -1) {
        onPageChange(currentPageNumber, total);
      } else {
        onPageChange(currentPageNumber, iPerPage);
      }
    }
    const pagesControl = [];
    for (let pageId = showingPageFromPage; pageId <= showingPageToPage; pageId++) {
      pagesControl.push(pageId);
    }

    this.setState({
      showingPageToPage,
      showingPageFromPage,
      pagesControl,
      totalPages,
      currentPageNumber,
      inputPageNumber: currentPageNumber,
      iPerPage,
    });
  }

  onItemPerPageChange(event) {
    const itemPerPage = parseInt(event.target.value);
    this.onPageChanged(false, false, false, itemPerPage);
  }

  onNextPage() {
    const { currentPageNumber, totalPages } = this.state;
    if (currentPageNumber < totalPages) {
      this.onPageChanged(currentPageNumber + 1);
      if(window.scrollBody) {
        window.scrollBody.scrollTop(0);
      }
    }

  }

  onBackPage() {
    const { currentPageNumber, totalPages } = this.state;
    const newPage = currentPageNumber - 1;
    if (newPage > 0) {
      this.onPageChanged(newPage);
      if(window.scrollBody) {
        window.scrollBody.scrollTop(0);
      }
    }

  }

  onGoLastPage() {
    const { totalPages, startAtPage } = this.state;
    if (totalPages) {
      let from = totalPages - this.getTotalPagerControl();
      if (from < startAtPage) {
        from = startAtPage;
      }
      this.onPageChanged(totalPages, from, totalPages);
    }
  }

  onGoFirstPage() {
    const { startAtPage, totalPages } = this.state;
    if (totalPages) {
      this.onPageChanged(startAtPage, startAtPage, this.getTotalPagerControl());
    }
  }

  handlePageInputOnChange(event) {
    let value = event.target.value;
    if(!value || !value.trim()) {
      return;
    }
    if(parseInt(value) > this.state.totalPages) {
      value = this.state.totalPages;
    }
    this.setState({ inputPageNumber: value, currentPageNumber: value });
  }

  handlePageInputOnKeyUp(event) {
    const keyCode = event.which || event.keyCode;
    if (keyCode !== 13) {
      return;
    }
    let { showingPageToPage, showingPageFromPage, startAtPage, currentPageNumber, totalPages } = this.state;
    let pageNumber = event.target.value;

    if (!pageNumber) {
      return;
    }
    pageNumber = parseInt(pageNumber);
    // if (pageNumber === currentPageNumber) {
    //   // this.setState({value: currentPageNumber});
    //   return;
    // }
    if (pageNumber < startAtPage) {
      pageNumber = startAtPage;
    }
    if (pageNumber > totalPages) {
      pageNumber = totalPages;
    }
    if (showingPageToPage < pageNumber) {
      showingPageToPage = pageNumber;
      showingPageFromPage = showingPageToPage - this.getTotalPagerControl();
    } else if (pageNumber < showingPageFromPage) {
      showingPageFromPage = pageNumber;
      showingPageToPage = showingPageFromPage + this.getTotalPagerControl();
    }
    this.onPageChanged(pageNumber, showingPageFromPage, showingPageToPage);
    if(window.scrollBody) {
      window.scrollBody.scrollTop(0);
    }
  }

  calculatePageTotal() {
    let { total, perPage } = this.props;
    // perPage = perPage || 10;
    // total = 100;
    let totalPages = Math.ceil(total / perPage);
    // if (total % perPage > 0) {
    //   totalPages += 1;
    // }
    this.setState({ totalPages });
    return totalPages;
  }

  onPageChanged(currentPageId, startFrom, endAt, itemPerPageChanged) {
    const { onPageChange } = this.props;
    const { currentPageNumber, iPerPage } = this.state;
    this.generateControlOfPageNumbers(currentPageId, startFrom, endAt, itemPerPageChanged);
  }

  onItemPerPageChanged(event, index, value) {
    this.setState({ value });
  }

  render() {
    const {
      currentPageNumber,
      pagesControl,
      totalPages,
      inputPageNumber,
      showingPageFromPage,
      showingPageToPage,
      startAtPage,
      iPerPage,
    } = this.state;


    let { itemPerPage, type } = this.props;
    // itemPerPage = itemPerPage || [10, 20, 30, 40, 50, 100];
    type = type || 'full';
    return (
      <div>
        {pagesControl && type === 'full' &&
        <FullPaginationType
          onGoFirstPage={this.onGoFirstPage}
          onBackPage={this.onBackPage}
          onPageChanged={this.onPageChanged}
          onNextPage={this.onNextPage}
          onGoLastPage={this.onGoLastPage}
          handlePageInputOnChange={this.handlePageInputOnChange}
          handlePageInputOnKeyUp={this.handlePageInputOnKeyUp}
          onItemPerPageChange={this.onItemPerPageChange}
          showingPageFromPage={showingPageFromPage}
          startAtPage={startAtPage}
          iPerPage={iPerPage}
          currentPageNumber={currentPageNumber}
          pagesControl={pagesControl}
          showingPageToPage={showingPageToPage}
          totalPages={totalPages}
          itemPerPage={itemPerPage}
        />
        }

        {pagesControl && type === 'shortest' &&
        <ShortestPaginationType
          onGoFirstPage={this.onGoFirstPage}
          onBackPage={this.onBackPage}
          onPageChanged={this.onPageChanged}
          onNextPage={this.onNextPage}
          onGoLastPage={this.onGoLastPage}
          handlePageInputOnChange={this.handlePageInputOnChange}
          handlePageInputOnKeyUp={this.handlePageInputOnKeyUp}
          onItemPerPageChange={this.onItemPerPageChange}
          showingPageFromPage={showingPageFromPage}
          startAtPage={startAtPage}
          iPerPage={iPerPage}
          inputPageNumber={inputPageNumber}
          currentPageNumber={currentPageNumber}
          pagesControl={pagesControl}
          showingPageToPage={showingPageToPage}
          totalPages={totalPages}
          itemPerPage={itemPerPage}
        />
        }
      </div>
    );
  }
}

Pagination.childContextTypes = {
  muiTheme: React.PropTypes.object.isRequired,
};

export default Pagination;
