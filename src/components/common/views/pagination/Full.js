import React from "react";

/**
 * Created by Trung Anh
 * created date 22/04/2017
 **/
class Full extends React.Component {

  render() {
    const {
      onGoFirstPage,
      showingPageFromPage,
      startAtPage,
      iPerPage,
      onBackPage,
      currentPageNumber,
      pagesControl,
      onPageChanged,
      onNextPage,
      inputPageNumber,
      handlePageInputOnChange,
      onItemPerPageChange,
      itemPerPage,
      handlePageInputOnKeyUp,
      showingPageToPage,
      totalPages,
      onGoLastPage,
    } = this.props;
    return (
      <div className="ui-pagination clearfix">
        <ul className="pull-right">

          <li
            onClick={onGoFirstPage}
            className={(showingPageFromPage === startAtPage || iPerPage === -1) ? 'disable' : ''}
          >
            <i className="mi mi-skip-next mi-24 rotateY180" />

          </li>
          <li
            onClick={onBackPage}
            className={(currentPageNumber === startAtPage || iPerPage === -1) ? 'disable' : ''}
          >
            <i className="mi mi-play-arrow mi-24 rotateY180" />
          </li>
          {
            pagesControl.map((pageId) =>
              <li
                className={currentPageNumber === pageId ? 'active' : ''}
                key={`page-${pageId}`} onClick={() => {
                  onPageChanged(pageId);
                }}
              >
                <i className="pageNumber"> {pageId} </i>
              </li>,
            )
          }

          <li
            onClick={onNextPage}
            className={(currentPageNumber === totalPages || iPerPage === -1) ? 'disable' : ''}
          >
            <i className="mi mi-play-arrow mi-24" />
          </li>
          <li
            onClick={onGoLastPage}
            className={(showingPageToPage === totalPages || iPerPage === -1) ? 'disable' : ''}
          >
            <i className="mi mi-skip-next mi-24" />
          </li>
          <li>
            <span>Page</span>
            <input
              placeholder={currentPageNumber} value={inputPageNumber}
              onKeyUp={handlePageInputOnKeyUp} onChange={handlePageInputOnChange}
            />
            <span>/{totalPages},</span>
          </li>
          <li>
            <select name="itemPerPage" value={iPerPage} onChange={onItemPerPageChange}>
              {
                itemPerPage.map((item) =>
                  <option value={item} key={item}>{item}</option>,
                )
              }
              <option value={-1}>all</option>
            </select>
            <span>Items</span>
          </li>
        </ul>
      </div>
    );
  }
}

export default Full;
