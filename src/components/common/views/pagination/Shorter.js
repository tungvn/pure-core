import React from "react";
import SvgIcon, {arrowIcon} from "../../../../common/icons/pixelz-svg";
import {multiLanguage, t4} from "i18n";
import {connect} from 'react-redux';

/**
 * Created by Trung Anh
 * created date 22/04/2017
 **/
class Shortest extends React.Component {

  setKeyboardForPaging = () => {
    const {dispatch} = this.props;
    dispatch({type: 'PAGING_KEYBOARD', isPagingKeyboard: true});
  }

  removeKeyboardForPaging = () => {
    const {dispatch} = this.props;
    dispatch({type: 'PAGING_KEYBOARD', isPagingKeyboard: false});
  }

  render() {
    const {
      onBackPage,
      currentPageNumber,
      onNextPage,
      inputPageNumber,
      handlePageInputOnChange,
      handlePageInputOnKeyUp,
      totalPages,
      intl,
    } = this.props;

    return (
      <div className="ui-pagination-shortest">
        <a className={`btn no-selectable ${currentPageNumber <= 1 ? 'disabled' : ''}`} onClick={onBackPage}>
          <SvgIcon path={arrowIcon} className="is-icon-add-1 m-r-10"/>
          <span className="no-selectable">{t4(intl, 'previous')}</span>
        </a>
        <input
          placeholder={currentPageNumber}
          onKeyUp={handlePageInputOnKeyUp}
          onChange={handlePageInputOnChange}
          onFocus={this.setKeyboardForPaging}
          onBlur={this.removeKeyboardForPaging}
          type="number"
          value={inputPageNumber}
          className="m-r-10"
        />
        <span className="m-r-10 total no-selectable">
          of {totalPages}
        </span>

        <a className={`btn next no-selectable ${currentPageNumber >= totalPages ? 'disabled' : ''}`} onClick={onNextPage}>
          <span className="no-selectable">{t4(intl, 'next')}</span>
          <SvgIcon path={arrowIcon} className="is-icon-add-1 m-l-10"/>
        </a>

      </div>
    );
  }
}

export default multiLanguage(connect()(Shortest));
