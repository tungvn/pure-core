export const userLoginKey = {
  USER_INFO: 'info',
  USER_KEY: 'user',
  USER_ACTIVE_REMEMBER_MODE: 'USER_ACTIVE_REMEMBER_MODE',
  USER_OAUTH: 'oauth',
  USER_AGENCY: 'agency',
}

export const storeUserInfoToLocalStorage = (key, data) => {
  let user = getUserFromLocalStorage();
  user[key] = data;
  let rememberMe = getDefaultRememberMeValue();
  if (rememberMe) {
    localStorage.setItem(userLoginKey.USER_KEY, JSON.stringify(user));
  } else {
    sessionStorage.setItem(userLoginKey.USER_KEY, JSON.stringify(user));
  }
}

export const getUserFromLocalStorage = () => {

  let user = {}
  let rememberMe = getDefaultRememberMeValue();
  if (rememberMe) {
    user = JSON.parse(localStorage.getItem(userLoginKey.USER_KEY));
  } else {
    user = JSON.parse(sessionStorage.getItem(userLoginKey.USER_KEY));
  }

  if (
    user &&
    user[userLoginKey.USER_OAUTH]
  ) {
    return user;
  }

  localStorage.removeItem(userLoginKey.USER_KEY);
  sessionStorage.removeItem(userLoginKey.USER_KEY);

  return {};

}

export const cleanUserFromLocalStorage = () => {
  localStorage.removeItem(userLoginKey.USER_KEY);
  localStorage.removeItem(userLoginKey.USER_ACTIVE_REMEMBER_MODE);
  sessionStorage.removeItem(userLoginKey.USER_KEY);
  sessionStorage.removeItem(userLoginKey.USER_ACTIVE_REMEMBER_MODE);
}

export const getDefaultRememberMeValue = ()=>{
  let rememberMe = localStorage.getItem(userLoginKey.USER_ACTIVE_REMEMBER_MODE);
  if(!rememberMe || rememberMe === 'false') {
    return false;
  }
  return true
}

//
// {
//   info: {},
//   oauth: {},
//   agency: {}
// }