import React from 'react';
import { connect } from 'react-redux';
import LoginForm from 'components/user/auth/login/Login';
import RegisterForm from 'components/user/auth/register/Register';
import DialogNoHeader from 'components/common/forms/elements/custom-popup/DialogNoHeader';
import { openLoginDialog as openLoginDialogAction, activeLoginTab, closeLoginDialog } from '../actions';

/**
 * Created by Peter Hoang Nguyen
 * Email: vntopmas@gmail.com
 * Tel: 0966298666
 * created date 01/04/2017
 **/
class LoginDialog extends React.Component {
  constructor(props) {
    super(props);
    this.openLoginPopup = this.openLoginPopup.bind(this);
    this.closeLoginPopup = this.closeLoginPopup.bind(this);
  }

  componentWillMount() {
    const { dispatch } = this.props;
    dispatch(activeLoginTab());
  }

  openLoginPopup() {
    const { dispatch } = this.props;
    dispatch(openLoginDialogAction());
  }

  closeLoginPopup() {
    const { dispatch } = this.props;
    dispatch(closeLoginDialog());
  }

  render() {
    const { openLoginDialog, isLoginTabActivated } = this.props;

    return (
      <DialogNoHeader
        modal
        closeOn={this.closeLoginPopup}
        open={openLoginDialog}
      >
        {
          isLoginTabActivated ? <LoginForm /> : <RegisterForm />
        }
      </DialogNoHeader>
    );
  }
}

const populateStateToProps = (state) => {
  const openLoginDialog = state.user.openLoginDialog;
  const isLoginTabActivated = state.user.isLoginTabActivated;
  return {
    openLoginDialog,
    isLoginTabActivated,
  };
};

LoginDialog.childContextTypes = {
  muiTheme: React.PropTypes.object.isRequired,
};

export default connect(populateStateToProps)(LoginDialog);
