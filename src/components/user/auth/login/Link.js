import React from 'react';
import { multiLanguage, t1 } from 'i18n';
import { connect } from 'react-redux';
import FlatButton from 'material-ui/FlatButton';
import { activeLoginTab, openLoginDialog } from '../actions';

/**
 * Created by Peter Hoang Nguyen
 * Email: vntopmas@gmail.com
 * Tel: 0966298666
 * created date 01/04/2017
 **/

class LoginLink extends React.Component {
  constructor(props) {
    super(props);
    this.openLoginPopup = this.openLoginPopup.bind(this);
  }

  componentWillMount() {
    const { dispatch } = this.props;
    dispatch(activeLoginTab());
  }

  openLoginPopup() {
    const { dispatch } = this.props;
    dispatch(openLoginDialog());
  }

  render() {
    const { intl } = this.props;
    const label = t1(intl, 'Login');
    return (
      // <a href="#" onClick={this.openLoginPopup} alt={label}> {label}</a>
      <FlatButton title={label} onClick={this.openLoginPopup}>
        <i className="fa fa-sign-in" />
      </FlatButton>
    );
  }
}

export default connect()(multiLanguage(LoginLink));
