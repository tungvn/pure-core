import React from 'react';
import { connect } from 'react-redux';
import FlatButton from 'material-ui/FlatButton';

/**
 * Created by Peter Hoang Nguyen
 * Email: vntopmas@gmail.com
 * Tel: 0966298666
 * created date 13/04/2017
 **/

class SocialAuth extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillMount() {
    const { dispatch } = this.props;
  }

  render() {
    return (
      <div className="center-block tools">
        <FlatButton href="#" className="another-login-icon facebook">
          <i className="fa fa-facebook" aria-hidden="true" />
        </FlatButton>
        <FlatButton href="#" className="another-login-icon googleplus">
          <i className="fa fa-google-plus" aria-hidden="true" />
        </FlatButton>
      </div>
    );
  }
}

SocialAuth.childContextTypes = {
  muiTheme: React.PropTypes.object.isRequired,
};

export default connect()(SocialAuth);
