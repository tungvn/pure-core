import React from "react";
import {connect} from "react-redux";
import {multiLanguage, t1} from "i18n";
import Helmet from "react-helmet";
import getColorConfigs from "common/css/generateCss";
import {userLoginKey, storeUserInfoToLocalStorage, getDefaultRememberMeValue} from "components/user/auth/common";
import ImageBackground from "components/common/views/image-background";
import LayoutHelper, {LayoutRegistered} from "layouts/LayoutHelper";
import Configs from "configs/configuration";
import {history} from "store";
import {setRequestingStatus} from "layouts/actions";
import {PixelInput, PCheckBox} from "components/common/forms/elements";
// import {setRejectInfomationFormStatus, setShowQuickViewStatus} from "components/collection/quickview/actions";
import {reduxForm} from "redux-form";
import {onOauth2Login, getOauth2UserInfo, getOauth2AgencyInfo} from "components/user/auth/oauth2/actions";
import "./stylesheet.css";
import {openMessageAlert} from "layouts/pixelz/actions";
import {getUserFromLocalStorage} from "components/user/auth/common/index";
import {isUserTokenValid} from "common/index";

/**
 * Created by Peter Hoang Nguyen
 * Email: vntopmas@gmail.com
 * Tel: 0966298666
 * created date 26/05/2017
 **/
class LoginPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillMount() {
    const user = getUserFromLocalStorage();
    if(user &&  isUserTokenValid(user.oauth)){
      window.location.href = '/';
    }

    LayoutHelper.useLayout(LayoutRegistered.defaultLayout, this);
    const {dispatch, tabNumber} = this.props;
    this.setState({ mounted: true });
    // dispatch(setShowQuickViewStatus(false, tabNumber));
    // dispatch(setRejectInfomationFormStatus(false));
    // localStorage.removeItem(userLoginKey.USER_ACTIVE_REMEMBER_MODE);
  }

  componentWillUnmount() {
    LayoutHelper.useLayout(LayoutRegistered.pixelz, this);
    this.setState({ mounted: false });
  }

  handleLoginAction = () => {
    storeUserInfoToLocalStorage();
    const { loginForm } = this.props;
    this.doLogin(
      { ...Configs.oauth2Configs, ...loginForm.values },
    );
  }

  doLogin = (data) => {
    const { dispatch, redirectUrlAfterLogin, intl } = this.props;
    dispatch(setRequestingStatus('loginBtn', true));
    const rememberMe = getDefaultRememberMeValue()?"true":"false";
    dispatch(onOauth2Login(
      Configs.apiUrl.oauthToken(rememberMe), data,
      () => {
        setTimeout(() => {
          dispatch(getOauth2UserInfo());
          dispatch(getOauth2AgencyInfo());
          window.initCollectionInFirstTime = true;
          if (this.state.mounted && this.state.msg) {
            this.setState({ msg: undefined });
          }
          if(redirectUrlAfterLogin && redirectUrlAfterLogin.indexOf('login') === -1) {
            history.push(redirectUrlAfterLogin);
          } else {
            history.push('/');
          }
          localStorage.setItem('doLogin',"doLogin");
          localStorage.removeItem('doLogin');
        }, 1);
      },
      () => {
        history.push('/login');
        dispatch(setRequestingStatus('loginBtn', false));
        dispatch(openMessageAlert({
          show: true,
          header: t1(intl, 'login'),
          message: t1(intl, 'your_username_or_password_is_not_valid'),
        }));
        // setTimeout(() => {
        //   dispatch(openMessageAlert({show: false}));
        // }, 5000);

      },
    ));
  }

  handleOnKeyDown = (event) => {
    const keyCode = event.keyCode;
    if (keyCode === 13) {
      this.handleLoginAction();
    }
  }

  onRememberChanged = (value) => {
    localStorage.setItem(userLoginKey.USER_ACTIVE_REMEMBER_MODE, value);
  }

  render() {
    const { screenSize, intl, siteConfigs, dispatch, isRequesting } = this.props;
    const { msg } = this.state;
    const loginBgImage = siteConfigs.login_background;
    const agencyName = siteConfigs.agency_name;

    const configsColor = getColorConfigs(siteConfigs.color_scheme);
    return (
      <ImageBackground src={loginBgImage} {...screenSize} bgClassName="bgClassName">
        <Helmet>
          <title>{agencyName}</title>
          <link type="image/x-icon" href={siteConfigs.favicon} rel="icon"/>
        </Helmet>

        <div className="ui-login-page" style={{ height: screenSize.height }}>
          <div className="ui-bg" />
          <div className="p-l-50 m-l-20 p-r-50 m-r-20 p-t-50 sm-p-l-15 sm-p-r-15 sm-p-t-40">

            <div className="ui-panel">
              <h1 className="brand">
                {agencyName}
              </h1>
              <div className="p-t-15 p-b-15">
                <span className="header">
                  <div>{t1(intl, 'sign_into_your_account')}</div>

                </span>

              </div>
              <div className="ui-login-fail">{msg}</div>
              <PixelInput
                onKeyDown={this.handleOnKeyDown}
                name="username" label={t1(intl, 'login')} placeholder={t1(intl, 'user_name')}
              />
              <PixelInput
                onKeyDown={this.handleOnKeyDown}
                type="password" name="password" label={t1(intl, 'password')} placeholder={t1(intl, 'credentials')}
              />
              <div className="p-t-10">
                <PCheckBox
                  color={siteConfigs.color_scheme}
                  name="rememberMe"
                  defaultValue={getDefaultRememberMeValue()}
                  onChanged={this.onRememberChanged}
                  lclassName="light" className="text-normal" label={t1(intl, 'keep_me_signed_in')}
                />
              </div>
              <div className="item-panel">
                <button
                  disabled={isRequesting}
                  className={`button-default button ${isRequesting ? 'disabled' : ''}`}
                  onClick={this.handleLoginAction}
                >
                  {
                    !isRequesting && <span>
                    {t1(intl, 'sign_in')}
                  </span>
                  }
                  {
                    isRequesting && <img width={40} height="auto" src="/media/images/svg-loaders/three-dots.svg"/>
                  }

                </button>
              </div>

            </div>
          </div>
        </div>
      </ImageBackground>
    );
  }
}

const mapPropsToState = (state) => ({
  screenSize: state.layout.screenSize,
  loginForm: state.form.loginForm,
  tabNumber: state.common.showQuickViewTabNumber,
  redirectUrlAfterLogin: state.common.redirectUrlAfterLogin,
  siteConfigs: state.common.siteConfigs || {},
  isRequesting: state.common.requestings['loginBtn']
});

export default reduxForm({
  form: 'loginForm',
})(connect(mapPropsToState)(multiLanguage(LoginPage)));

