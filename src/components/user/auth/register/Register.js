import React from "react";
import {connect} from "react-redux";
import {multiLanguage, t1} from "i18n";
import {reduxForm} from "redux-form";
import FlatButton from "material-ui/FlatButton";
import RaisedButton from "material-ui/RaisedButton";
import AuthPanel from "components/user/auth/AuthPanel";
import {TextField, Checkbox} from "components/common/forms/elements";
import {activeLoginTab, activeRegisterTab} from "components/user/auth/actions";
import "../stylesheet.css";
import SocialAuth from "../social-auth/SocialAuth";

/**
 * Created by Peter Hoang Nguyen
 * Email: vntopmas@gmail.com
 * Tel: 0966298666
 * created date 30/03/2017
 **/

class Register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.doRegister = this.doRegister.bind(this);
  }

  componentWillMount() {
    const { dispatch } = this.props;
    dispatch(activeRegisterTab());
  }

  doRegister() {
    const { registerForm } = this.props;
    // let data = Object.assign({},registerForm.values, {submit : 1});
    // Fetch.post("/user/register", data);
    fetch(`http://vlms.dev/user/register?submit=1&&name=${registerForm.values.name}&pass=${registerForm.values.pass}&mail=${registerForm.values.email}`);
    // Request.post("/user/register", registerForm.values);
    // Request.post("/user/login", loginForm.values)
  }

  render() {
    const { intl, dispatch } = this.props;
    return (

      <AuthPanel>
        <div className="ui-auth-panel ui-register">
          <div className="ui-auth-header">
            <FlatButton
              onClick={() => {
                dispatch(activeLoginTab());
              }}
            >
              { t1(intl, 'Login')}

            </FlatButton>
            <span>/</span>
            <a className="active">
              { t1(intl, 'Register') }
            </a>
          </div>
          <TextField
            fullWidth name="name"
            hintText={t1(intl, 'fullname')}
            floatingLabelText={t1(intl, 'fullname')}
          />

          <TextField
            fullWidth name="email"
            hintText={t1(intl, 'email')}
            floatingLabelText={t1(intl, 'email')}
          />

          <TextField
            fullWidth name="pass"
            hintText={t1(intl, 'Password')}
            floatingLabelText={t1(intl, 'Password')}
          />

          <div className="terms-and-conditions clearfix">
            <div className="pull-left">
              <Checkbox
                labelStyle={{ color: '#9d9d9d' }}
                iconStyle={{ fill: '#9d9d9d' }}
                style={{ display: 'inline-block', width: 'auto' }}
                name="remember_me"
              />
            </div>
            <div className="pull-left text-link">
              { t1(intl, 'agree_with')}
              <FlatButton href="#"> { t1(intl, 'terms')}</FlatButton>
              { ' & ' }
              <FlatButton href="#"> { t1(intl, 'conditions')}</FlatButton>
            </div>
          </div>

          <div className="ui-button-group center-block">
            <RaisedButton label={t1(intl, 'Register')} onClick={this.doRegister} className="button" primary />
          </div>
          <div className="another-register-tools-panel">
            <div className="header clearfix">
              <div className="center-block line-over">
                <span>{ t1(intl, 'or_register_with') }</span>
              </div>
            </div>
            <SocialAuth />
          </div>

        </div>
      </AuthPanel>
    );
  }
}

const populateStateToProps = (state) => ({
  registerForm: state.form.register,
});

Register.childContextTypes = {
  muiTheme: React.PropTypes.object.isRequired,
};

const RegisterForm = reduxForm({
  form: 'register',  // a unique identifier for this form
})(multiLanguage(Register));

export default connect(populateStateToProps)(RegisterForm);
