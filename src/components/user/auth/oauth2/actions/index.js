/**
 * Created by Peter Hoang Nguyen on 3/17/2017.
 */
export const ON_OAUTH2_LOGIN_ACTION = 'ON_OAUTH2_LOGIN_ACTION';
export const ON_OAUTH2_RELOGIN_ACTION = 'ON_OAUTH2_RELOGIN_ACTION';
export const ON_OAUTH2_LOGIN_SUCCESS_ACTION = 'ON_OAUTH2_LOGIN_SUCCESS_ACTION';
export const ON_OAUTH2_RELOGIN_SUCCESS_ACTION = 'ON_OAUTH2_RELOGIN_SUCCESS_ACTION';
export const ON_OAUTH2_GET_USER_INFO = 'ON_OAUTH2_GET_USER_INFO';
export const ON_OAUTH2_GET_AGENCY_INFO = 'ON_OAUTH2_GET_AGENCY_INFO';
export const ON_OAUTH2_GET_AGENCY_INFO_SUCCESS = 'ON_OAUTH2_GET_AGENCY_INFO_SUCCESS';
export const ON_OAUTH2_LOGOUT_ACTION = 'ON_OAUTH2_LOGOUT_ACTION';
export const ON_OAUTH2_LOGOUT_SUCCESS_ACTION = 'ON_OAUTH2_LOGOUT_SUCCESS_ACTION';
export const LOGOUT_ACTION = 'LOGOUT_ACTION';

export function onOauth2Login(url, params, onSuccess, onFail) {
  return { type: ON_OAUTH2_LOGIN_ACTION, url, params, onSuccess, onFail };
}

export function onOauth2Relogin(url, params, onSuccess, onFail) {
  return { type: ON_OAUTH2_RELOGIN_ACTION, url, params, onSuccess, onFail };
}


export function logout(redirectUrl, params, onSuccess, onFail) {
  return { type: LOGOUT_ACTION, redirectUrl, params, onSuccess, onFail };
}

export function getOauth2UserInfo() {
  return { type: ON_OAUTH2_GET_USER_INFO };
}

export function getOauth2AgencyInfo() {
  return { type: ON_OAUTH2_GET_AGENCY_INFO };
}

export function getOauth2AgencyInfoSuccess(agency) {
  return { type: ON_OAUTH2_GET_AGENCY_INFO_SUCCESS, agency };
}

export function onOauth2LoginSuccess(oauth) {
  return { type: ON_OAUTH2_LOGIN_SUCCESS_ACTION, oauth };
}

export function onOauth2ReloginSuccess() {
  return { type: ON_OAUTH2_RELOGIN_SUCCESS_ACTION };
}


export function onOauth2Logout(redirectUrl) {
  return { type: ON_OAUTH2_LOGOUT_ACTION, redirectUrl };
}

export function onOauth2LogoutSuccess(userInfo) {
  return { type: ON_OAUTH2_LOGOUT_SUCCESS_ACTION, userInfo };
}
