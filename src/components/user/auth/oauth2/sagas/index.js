import {takeEvery} from "redux-saga";
import {userLoginKey, storeUserInfoToLocalStorage, cleanUserFromLocalStorage} from "components/user/auth/common";
import {call, put, fork} from "redux-saga/effects";
import Requester from "common/network/http/Request";
import Configs from "configs/configuration";
import {loginSuccess} from "components/user/auth/actions";
import {
  ON_OAUTH2_LOGIN_ACTION,
  LOGOUT_ACTION,
  ON_OAUTH2_RELOGIN_ACTION,
  ON_OAUTH2_GET_AGENCY_INFO,
  ON_OAUTH2_GET_USER_INFO,
  onOauth2LoginSuccess,
  getOauth2AgencyInfoSuccess
} from "../actions";

function* getAgencyInfo() {
  const agencyInfo = yield call(
    Requester.get,
    Configs.apiUrl.agencyInfo,
  );
  if (agencyInfo && agencyInfo.success) {
    storeUserInfoToLocalStorage(userLoginKey.USER_AGENCY, {...agencyInfo.result});
    yield put(getOauth2AgencyInfoSuccess({...agencyInfo.result}));
  }
}

function* getUserInfo() {
  const userInfo = yield call(
    Requester.get,
    Configs.apiUrl.userInfo,
  );
  if (userInfo && userInfo.success) {
    storeUserInfoToLocalStorage(userLoginKey.USER_INFO, {...userInfo.result});
    yield put(loginSuccess({...userInfo.result}));
    const language = userInfo.result['language'];
    if(!language || !language['language_iso_code'] || ! language['language_id']) {
      return;
    }

    yield put({type: 'SET_LANGUAGE', language});
  }
}

function* login(action) {
  const {url, params, onSuccess, onFail} = action;
  let user_info;
  if(params) {
    user_info = params.user_info;
    delete params.user_info;
  }

  const response = yield call(
    Requester.post,
    url,
    params,
  );
  if (response && response.access_token) {
    const serverExpriedIn = response.expires_in || 0;
    response.timeExpriedAt = (serverExpriedIn - 200) * 1000 + new Date().getTime(); // lấy độ trễ là 200s
    let oauthData = {...response};
    if(!oauthData.user_info) {
      oauthData = {...oauthData, user_info};
    }
    storeUserInfoToLocalStorage(userLoginKey.USER_OAUTH, oauthData);
    yield put(onOauth2LoginSuccess(oauthData));
    if (onSuccess) {
      onSuccess(loginSuccess());
    }
    return;
  }

  if (onFail && typeof onFail === 'function') {
    onFail();
  }
}

//
// function* relogin(action) {
//   const {url, params, onSuccess, onFail} = action;
//   const response = yield call(
//     Requester.post,
//     url,
//     params,
//   );
//   if (response && response.success) {
//     yield put(onOauth2ReloginSuccess());
//     if (onSuccess) {
//       onSuccess();
//     }
//     return;
//   }
//
//   if (onFail) {
//     onFail();
//   }
// }


function* logout(action) {
  const {redirectUrl, params, onSuccess, onFail} = action;
  // const response = yield call(
  //   Requester.post,
  //   url,
  //   params,
  // );
  // if (response.access_token) {
  if (true) {
    // const serverExpriedIn = response.expires_in || 0;
    // response.timeExpriedAt = (serverExpriedIn - 200) * 1000 + new Date().getTime(); //lấy độ trễ là 200s
    yield put(onOauth2LoginSuccess({}));
    yield put(getOauth2AgencyInfoSuccess({}));
    cleanUserFromLocalStorage();
    //inform to other tabs about logout action
    localStorage.setItem('doLogout','doLogout');
    localStorage.removeItem('doLogout');
    if (onSuccess) {
      onSuccess();
    }
    return;
  }

  if (onFail) {
    onFail();
  }
}

export const onOauth2ReloginAction = function* reloginSaga() {
  yield* takeEvery(ON_OAUTH2_RELOGIN_ACTION, login);
};

export const onOauth2LoginAction = function* loginSaga() {
  yield* takeEvery(ON_OAUTH2_LOGIN_ACTION, login);
};

export const onOauth2GetUserInfotAction = function* getUserInfoSaga() {
  yield* takeEvery(ON_OAUTH2_GET_USER_INFO, getUserInfo);
};

export const onOauth2LogoutAction = function* logoutSaga() {
  yield* takeEvery(LOGOUT_ACTION, logout);
};

export const onOauth2GetAgencyInfoAction = function* getAgencySaga() {
  yield* takeEvery(ON_OAUTH2_GET_AGENCY_INFO, getAgencyInfo);
};

export default [
  fork(onOauth2LoginAction),
  fork(onOauth2ReloginAction),
  fork(onOauth2LogoutAction),
  fork(onOauth2GetUserInfotAction),
  fork(onOauth2GetAgencyInfoAction),
];
