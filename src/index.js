import React from "react";
import {render} from "react-dom";
import {Provider} from "react-redux";
import {ConnectedRouter} from "react-router-redux";
// import {IntlProvider} from "react-intl";
import IntlProvider from 'i18n/I18nComponent';
import getMuiTheme from "material-ui/styles/getMuiTheme";
import injectTapEventPlugin from "react-tap-event-plugin";
import {MuiThemeProvider, lightBaseTheme} from "material-ui/styles";
import appRouters from "routes";
import {Switch} from "react-router-dom";
import {renderRoutes} from "react-router-config";
import Store, {history} from "./store";
import {i18nIntegration} from "./i18n";

injectTapEventPlugin();

const lightMuiTheme = getMuiTheme(lightBaseTheme);

lightMuiTheme.menuItem = {
  ...lightMuiTheme.menuItem,
  dataHeight: 32,
  height: 38,
  padding: 10,
};

window.rootAppContainer = document.getElementById('root');

render(
  <Provider store={Store}>
    <IntlProvider>
      <MuiThemeProvider muiTheme={lightMuiTheme}>
        <ConnectedRouter history={history}>
          <Switch>
            {renderRoutes(appRouters)}
          </Switch>
        </ConnectedRouter>
      </MuiThemeProvider>
    </IntlProvider>
  </Provider>
  ,
  window.rootAppContainer,
);
