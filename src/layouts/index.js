import React from "react";
import {connect} from "react-redux";
import {multiLanguage, t1, t4} from "i18n";
import Popup from "components/common/views/popup";
import {renderRoutes} from "react-router-config";
import Snackbar from 'material-ui/Snackbar';

import Configs from "configs/configuration";
import {refreshToken} from "common";
// import {redirectUrlAfterLogin, openMessageAlert} from "layouts/pixelz/actions";
import {history} from "store";
import Helmet from "react-helmet";
import getColorConfigs, {DEFAULT_COLOR} from 'common/css/generateCss';
import {logout} from "components/user/auth/oauth2/actions";
import {setLayout, windowResize} from "./actions";
import LayoutRegister from "./register";
import "./layout-common.css";
import {fetchSiteConfig} from 'layouts/actions';
import writeCss from './css';
import ReactDOM from "react-dom";
import {getUserFromLocalStorage} from "components/user/auth/common/index";

/**
 * Created by Peter Hoang Nguyen
 * Email: vntopmas@gmail.com
 * Tel: 0966298666
 * created date 25/04/2017
 **/
class Layouts extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      width: 0,
      height: 0,
    };
    this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
    this.initLayout = this.initLayout.bind(this);
    this.initLayout();
    this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if(this.props.showQuickView !== nextProps.showQuickView) {
      this.updateWindowDimensions(nextProps.showQuickView);
    }
    const {siteConfigs} = this.props;

    if(siteConfigs.color_scheme !== nextProps.siteConfigs.color_scheme) {
      const configsColor = nextProps.siteConfigs.color_scheme || DEFAULT_COLOR;
      window.NProgress.configure({
        template: `<div class="bar" style="background-color:${configsColor}" role="bar"><div class="peg" style="box-shadow: 0 0 10px ${configsColor}, 0 0 5px ${configsColor}"></div></div><div class="spinner" role="spinner"><div class="spinner-icon" style="border-top-color: ${configsColor}; border-left-color:${configsColor}"></div></div>`
      });
    }
  }

  componentDidMount() {
    this.updateWindowDimensions();
    const Timer = setInterval(this.autoResetToken, 1000);
    window.messagePopup = ReactDOM.findDOMNode(this.refs.messagePopup);
    this.setState({Timer});
    window.addEventListener('resize', this.updateWindowDimensions);

  }

  componentWillMount() {
    const {dispatch, siteConfigs} = this.props;
    dispatch(fetchSiteConfig());
    const configsColor = siteConfigs.color_scheme || DEFAULT_COLOR;
    window.NProgress.configure({
      template: `<div class="bar" style="background-color:${configsColor}" role="bar"><div class="peg" style="box-shadow: 0 0 10px ${configsColor}, 0 0 5px ${configsColor}"></div></div><div class="spinner" role="spinner"><div class="spinner-icon" style="border-top-color: ${configsColor}; border-left-color:${configsColor}"></div></div>`
    });
    // window.addEventListener("storage", this.sessionStorageTransfer);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateWindowDimensions);
    clearInterval(this.state.Timer);
    window.messagePopup = undefined;
  }

  // transfers sessionStorage from one tab to another
  sessionStorageTransfer = (event) => {
    if (!event) {
      event = window.event;
    } // ie suq
    if (!event.newValue) return;          // do nothing if no value to work with
    if (event.key === 'getSessionStorage') {
      // another tab asked for the sessionStorage -> send it
      localStorage.setItem('sessionStorage', JSON.stringify(sessionStorage));
      // the other tab should now have it, so we're done with it.
      localStorage.removeItem('sessionStorage'); // <- could do short timeout as well.
    } else if (event.key === 'sessionStorage' && !sessionStorage.length) {
      // another tab sent data <- get it
      const data = JSON.parse(event.newValue);
      for (let key in data) {
        sessionStorage.setItem(key, data[key]);
      }
    }
  }

  updateWindowDimensions(showQuickView) {
    const {dispatch} = this.props;
    if(showQuickView === undefined || typeof showQuickView !== "boolean") {
      showQuickView = this.props.showQuickView;
    }
    const screenSize = {
      width: window.innerWidth - (showQuickView === true ? 423 : 0),
      height: window.innerHeight - 1,
    };
    const bodyScreenSize = {
      width: window.innerWidth - Configs.leftSpace - 1 - (showQuickView  === true ? 423 : 0),
      height: window.innerHeight,
    };
    dispatch(windowResize(screenSize, bodyScreenSize));
  }

  autoResetToken = () => {
    const {dispatch, currentUrl} = this.props;
    const oauth = getUserFromLocalStorage().oauth;
    if (oauth && oauth.refresh_token && oauth.timeExpriedAt) {
      const timeExpriedAt = oauth.timeExpriedAt;
      if (parseInt(timeExpriedAt) <= new Date().getTime()) {
        // dispatch(redirectUrlAfterLogin(currentUrl));
        refreshToken({
          oauth,
          dispatch
        }, undefined,()=>{
          dispatch(logout('/login', undefined, () => {
            history.push('/login');
          }))});
      }
    }
  }


  initLayout() {
    let defaultlayoutId;
    const {dispatch} = this.props;

    for (const layoutId in LayoutRegister) {
      const layoutConfig = LayoutRegister[layoutId];
      if (layoutConfig.isDefault) {
        defaultlayoutId = layoutId;
      }
    }
    defaultlayoutId = defaultlayoutId || Object.keys(LayoutRegister)[0];
    dispatch(setLayout(defaultlayoutId));
  }

  // closeMessageAlert = () => {
  //   const {dispatch} = this.props;
  //   dispatch(openMessageAlert({show: false}));
  // }

  render() {
    const {currentLayout, children, route, siteConfigs, intl} = this.props;
    let Notify = this.props.Notify || {};
    writeCss(siteConfigs.color_scheme);
    const configsColor = getColorConfigs(siteConfigs.color_scheme);
    const noLayout = <div>{children}</div>;
    if (!currentLayout) {
      return noLayout;
    }

    const CurrentLayoutConfig = currentLayout ? LayoutRegister[currentLayout.layoutId] : false;
    if (!CurrentLayoutConfig || !CurrentLayoutConfig.component) {
      return noLayout;
    }
    const closeMessageButton = (
      <button onClick={this.closeMessageAlert} type="button"
              style={{...configsColor.backgroundColor, ...configsColor.borderColor}}
              className="btn btn-primary">{t1(intl, 'got_it')}</button>
    );
    return (
      <CurrentLayoutConfig.component {...CurrentLayoutConfig.params} {...this.props}>
        <Helmet>
          <link type="image/x-icon" href={siteConfigs.favicon} rel="icon"></link>
        </Helmet>
        {renderRoutes(route.routes)}

          <Popup
            ref="messagePopup"
            title={Notify.header || ''}
            open={Notify.show && !Notify.isSnack}
            onClose={this.closeMessageAlert}
            footer={closeMessageButton}
          >
            <p>
              {Notify.message || ''}
            </p>
          </Popup>

          <Snackbar
            className={"snackbar-zindex"}
            open={Notify.show && Notify.isSnack}
            bodyStyle={{backgroundColor: '#585858'}}
            message={Notify.message || ''}
            autoHideDuration={4000}
            onRequestClose={this.closeMessageAlert}
          />

      </CurrentLayoutConfig.component>
    );
  }
}

const mapStateToProps = (state) => {
  let currentUrl;
  if (state.routing && state.routing.location && state.routing.location.pathname) {
    currentUrl = state.routing.location.pathname;
  }
  return {
    currentLayout: state.layout,
    oauth: state.user.oauth,
    Notify: state.common.Notify,
    showQuickView: state.common.showQuickView,
    currentUrl,
    siteConfigs: state.common.siteConfigs || {}
  }
};

export default connect(mapStateToProps)(multiLanguage(Layouts));
