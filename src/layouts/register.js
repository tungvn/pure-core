import {LayoutRegistered} from "layouts/LayoutHelper";
import defaultLayout from "layouts/default";

export default {
  [LayoutRegistered.defaultLayout]: {
    component: defaultLayout,
    isDefault: true,
  }
};
