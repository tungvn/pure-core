import {sheet, DEFAULT_COLOR} from 'common/css/generateCss';
import {hexToRGBA} from 'common';

export default (color) => {
  const configColor = color || DEFAULT_COLOR;
  const bgColor = hexToRGBA(configColor);
  const bgColorHover = hexToRGBA(configColor, 0.8);

  sheet('site-css-config').innerHTML = `
      .ui-input-tags span.tag .remove::after { color: ${bgColor} !important; }
      .ui-input-tags .ui-input-tags:focus {border: 1px solid ${bgColor} !important;}
      .ui-login-page .button {background-color: ${bgColor}; border-color: ${bgColor}}
      .ui-login-page .button:hover {background-color: ${bgColorHover}}
      `;
}