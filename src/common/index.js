import Configs from "configs/configuration";
import {onOauth2Relogin} from "components/user/auth/oauth2/actions";
import Store from "store";
// import {setShowQuickViewStatus, setRejectInfomationFormStatus} from 'components/collection/quickview/actions';
// import {activeKeyboardEventForCollectionId} from "components/collection/actions";
import {collectionListId} from "configs/configuration";

export const isMobileBrowser = () => {
  let check = false;
  (function (a) {
    if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true;
  }(navigator.userAgent || navigator.vendor || window.opera));
  return check;
};

export const getTimeEnableForCacheConfig = () => {
  let timeToConfig = process.env.REACT_APP_TIME_TO_CACHING;
  if (timeToConfig) {
    try {
      timeToConfig = parseInt(timeToConfig);
    } catch (err) {
      timeToConfig = 12;
    }

    return timeToConfig * 60 * 60 * 1000;
  }
}

export const defaultMaxCollectionSlideValue = isMobileBrowser() ? 0.46 : 0.46;
export const defaultMinCollectionSlideValue = isMobileBrowser() ? 0.2 : 0.04;
export const defaultSlideValueConfig = isMobileBrowser() ? 0.4 : 0.18;

export const getParams = (props) => {
  const {match} = props;
  if (match && match.params) {
    return match.params;
  }
  return {};
};

export const getImage = (data, maxSize) => {
  // const maxSizeOfImage = maxSize || 2000;
  let fileKey = '';
  if (data.s3_file_key) {
    fileKey = data.s3_file_key.replace(/\\/g, '/');
  }
  // return 'http://ccdemo.pixelz.com.s3-website-eu-west-1.amazonaws.com/images/order/orderExample84.jpg';
  return `${Configs.apiUrl.imageServer}?bucket=${encodeURIComponent(data.s3_bucket)}&fileKey=${encodeURIComponent(fileKey)}&maxSize=${maxSize}`;
};

export const getS3Image = (s3FileKey, s3Bucket, maxSize) => {
  // const maxSizeOfImage = maxSize || 2000;
  let fileKey = '';
  if (s3FileKey) {
    fileKey = s3FileKey.replace(/\\/g, '/');
  }
  // return 'http://ccdemo.pixelz.com.s3-website-eu-west-1.amazonaws.com/images/order/orderExample84.jpg';
  return `${Configs.apiUrl.imageServer}?bucket=${encodeURIComponent(s3Bucket)}&fileKey=${encodeURIComponent(fileKey)}&maxSize=${maxSize}`;
};

export const convertByteToMegaByte = (input) => {
  let myInput = input || 0;
  myInput = parseFloat(myInput);
  if (myInput === 0) {
    return '';
  }
  let size = myInput / (1024 * 1024);
  return size.toFixed(2) + ' MB';
}

export const getOriginalImageSize = (width, height) => {
  const size = width > height ? width : height;
  // const windowSize = window.clientWidth > window.clientHeight ? window.clientWidth : window.clientHeight;
  // const doubleScreenSize = windowSize * 2;
  const doubleScreenSize = 1200;
  if (size > doubleScreenSize) {
    return doubleScreenSize;
  }
  return size;
}

export const getThumbnailImageSize = (originalSize) => {
  const defaultSize = 800;
  if (!originalSize) {
    return defaultSize;
  }
  return originalSize > defaultSize ? defaultSize : originalSize;
}

export const refreshToken = (prams, onSuccess, onFail) => {
  const {dispatch, oauth} = prams;

  const userSessionInfo = {
    ...Configs.oauth2ConfigsResetToken,
    refresh_token: oauth.refresh_token,
    user_info: oauth.user_info,
  };

  dispatch(onOauth2Relogin(
    Configs.apiUrl.oauthRevocationToken, {...userSessionInfo},
    onSuccess, onFail));

}

export const hexToRGBA = (hex, opacity) => {
  return 'rgba(' + (hex = hex.replace('#', '')).match(new RegExp('(.{' + hex.length/3 + '})', 'g')).map(function(l) { return parseInt(hex.length%2 ? l+l : l, 16) }).concat(opacity||1).join(',') + ')';
}

export const slectSpanTag = spanObject => {
  const range = document.createRange();
  range.selectNodeContents(spanObject);
  const sel = window.getSelection();
  sel.removeAllRanges();
  sel.addRange(range);
}

export const copyTextToClipboardWithoutFormating = text =>{
  const input = document.createElement('input');
  input.style.opacity = 0;
  input.setAttribute('value', text);
  document.body.appendChild(input);
  // select the contents
  input.select();
  document.execCommand("Copy");
  document.body.removeChild(input);
}

export const isUserTokenValid = oauth => {
  if (oauth && oauth.timeExpriedAt) {
    const timeExpriedAt = oauth.timeExpriedAt;
    if (parseInt(timeExpriedAt) <= new Date().getTime()) {
      return false
    }else{
      return true;
    }
  }
  return false;
}

export const handleShowQuickView = (tabNumber) => {
  const { dispatch } = Store;
  // dispatch(setShowQuickViewStatus(true,tabNumber));
}

export const handleHideQuickView = (tabNumber) => {
  const { dispatch } = Store;
  // dispatch(setShowQuickViewStatus(false, tabNumber));
  // dispatch(setRejectInfomationFormStatus(false));
  // dispatch(activeKeyboardEventForCollectionId(collectionListId.DETAIL_COLLECTION_LIST_ID));
  if(window.scrollBody) {
    window.scrollBody.scrollTop(0);
  }
}

export const clearDragImage = () => {
  let dragImages = document.getElementsByClassName("drag-collection_data");
  if(dragImages){
    for(let i =0; i < dragImages.length; i++){
      document.body.removeChild(dragImages[i]);
    }
  }
}