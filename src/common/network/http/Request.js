/**
 * Created by Peter Hoang Nguyen on 4/2/2017.
 */
import axios from "axios";
// import {redirectUrlAfterLogin} from "layouts/pixelz/actions";
import {onOauth2Relogin} from "components/user/auth/oauth2/actions";
import Configs from "configs/configuration";
import common from "./Common";
import Store, {history} from "store";
import {getUserFromLocalStorage, getDefaultRememberMeValue} from "components/user/auth/common";
const PROGRESS_PROCESS = 0.8;

axios.interceptors.request.use((config) =>
  // console.log("before request: success", config);
  config, (error) =>
  // console.log("before request: not success");
  Promise.reject(error));

// Add a response interceptor
axios.interceptors.response.use((response) =>
  // Do something with response data
  // console.log("response: success");
  response, (error) =>
  // Do something with response error
  // console.log("response: not success");
  Promise.reject(error));

const addCommonHeader = () => {
  let oauth = Store.getState().user.oauth || {};
  if(!oauth || Object.keys(oauth).length === 0 || !oauth.access_token) {
    oauth = getUser().oauth;
  }
  if (oauth && oauth.access_token) {
    axios.defaults.headers.common.Authorization = `Bearer ${oauth.access_token}`;
    if (oauth.user_info) {
      axios.defaults.headers.common['x-user-id'] = `${oauth.user_info.user_id}`;
    }
    // axios.defaults.headers.common['x-user-id'] = `ABE75151-6CC3-4790-9E11-762801EAC16A`;
  }
};


const addCommonZipHeader = () => {
  let oauth = Store.getState().user.oauth || {};
  if(!oauth || Object.keys(oauth).length === 0 || !oauth.access_token) {
    oauth = getUser().oauth;
  }
  if (oauth && oauth.access_token) {
    axios.defaults.headers.common.Authorization = `Bearer ${oauth.access_token}`;
  }
  axios.defaults.headers.common['content-type'] = 'application/json';
  axios.defaults.async = true;
  axios.defaults.responseType = 'arraybuffer';
  axios.defaults.crossDomain = true;
};

const getUser = () => {
  let user = getUserFromLocalStorage();
  if (!user || (user && Object.keys(user).length === 0 && user.constructor === Object)) {
    return {};
  }
  return user;
}

class Request {

  get(url, params, isRetry, onSuccess, onFail) {
    const {urlProcess, allParams} = common.getURL(url, params);
    const oauth = Store.getState().user.oauth || {};
    addCommonHeader();
    const that = this;
    window.NProgress.start();
    window.NProgress.set(PROGRESS_PROCESS);

    return axios.get(urlProcess, {
      params: allParams,
    }).then((response) => {
      window.NProgress.done();

      if (onSuccess) {
        onSuccess(response);
      }
      return (response && response.data) || {}
    }).catch((error) => {
      window.NProgress.done();

      const response = error.response || {};
      if (response && response.status === 401 && oauth.refresh_token && !isRetry) {
        const request = new Request();
        return request.redoNetworkAction(url, params, 'get');
      }
    });
  }

  getJSON(url, params, isRetry, onSuccess) {
    axios.defaults.headers.common['content-type'] = 'application/json';
    return this.get(url, params, isRetry, onSuccess);
  }

  post(url, params, isRetry, notUseHeader) {
    const {urlProcess, allParams} = common.getURL(url, params);
    const formPost = common.createFrom(allParams);
    const oauth = Store.getState().user.oauth || {};
    const that = this;
    delete axios.defaults.headers.common['content-type'];
    addCommonHeader();
    window.NProgress.start();
    window.NProgress.set(PROGRESS_PROCESS);
    return axios.post(urlProcess, formPost)
      .then((response) => {
        window.NProgress.done();
        return response.data || {}
      })
      .catch((error) => {
        window.NProgress.done();
        const response = error.response || {};
        if (response.status === 401 && oauth.refresh_token && !isRetry) {
          const request = new Request();
          return request.redoNetworkAction(url, params, 'post');
        }
      });
  }

  postAsBody(url, params, isRetry, onSuccess, onFail) {
    const {urlProcess} = common.getURL(url, params);
    const oauth = Store.getState().user.oauth || {};
    const that = this;
    addCommonHeader();
    window.NProgress.start();
    window.NProgress.set(PROGRESS_PROCESS);
    axios.defaults.headers.common['content-type'] = 'application/json';
    return axios.post(urlProcess, params)
      .then((response) => {
        window.NProgress.done();
        if (onSuccess) {
          onSuccess(response.data);
        }
        return response.data || {};
      })
      .catch((error) => {
        const response = error.response || {};
        window.NProgress.done();
        if (onFail) {
          onFail();
        }
        if (response.status === 401 && oauth.refresh_token && !isRetry) {
          const request = new Request();

          return request.redoNetworkAction(url, params, 'post');
        }
      });
  }

  postAsBodyNoOauth(url, params, isRetry, onSuccess, onFail) {
    const {urlProcess} = common.getURL(url, params);
    const oauth = Store.getState().user.oauth || {};
    const that = this;
    delete axios.defaults.headers.common['x-user-id'];
    delete axios.defaults.headers.common['Authorization'];
    axios.defaults.headers.common['content-type'] = 'application/json';
    window.NProgress.start();
    window.NProgress.set(PROGRESS_PROCESS);
    return axios.post(urlProcess, params)
      .then((response) => {
        window.NProgress.done();
        if (onSuccess) {
          onSuccess(response.data);
        }
        return response.data || {};
      })
      .catch((error) => {
        window.NProgress.done();
        const response = error.response || {};
        if (onFail) {
          onFail();
        }
        if (response.status === 401 && oauth.refresh_token && !isRetry) {
          const request = new Request();

          return request.redoNetworkAction(url, params, 'post');
        }
      });
  }

  zipFileAsPost(url, params, isRetry, onFail) {
    const {urlProcess} = common.getURL(url, params);
    addCommonZipHeader();
    return axios.post(urlProcess, params)
      .then((response) => response)
      .catch((error) => {
        if (onFail) {
          onFail(error);
        }
      });
  }

  zipFileAsGet(url, params, isRetry, onFail) {
    const {urlProcess} = common.getURL(url, params);
    addCommonZipHeader();
    return axios.get(urlProcess, params)
      .then((response) => response)
      .catch((error) => {
        if (onFail) {
          onFail(error);
        }
      });
  }

  redoNetworkAction(url, params, method) {
    const {oauth} = Store.getState().user;

    const userSessionInfo = {
      ...Configs.oauth2ConfigsResetToken,
      refresh_token: oauth.refresh_token,
    };
    const rememberMe = getDefaultRememberMeValue()?"true":"false";
    Store.dispatch(onOauth2Relogin(
      Configs.apiUrl.oauthToken(rememberMe), userSessionInfo,
      () => {
        setTimeout(() => {
          if (method === 'get') {
            return this.get(url, params, true);
          }
          return this.post(url, params, true);
        }, 1);
      },
      () => {
        let currentUrl;
        if (Store.getState().routing && Store.getState().routing.location && Store.getState().routing.location.pathname) {
          currentUrl = Store.getState().routing.location.pathname;
        }
        // Store.dispatch(redirectUrlAfterLogin(currentUrl));
        // history.push('/login');
      },
    ));
  }

}
export const readAllChunks = (readableStream) => {
  const reader = readableStream.getReader();
  const chunks = [];

  function pump() {
    return reader.read().then(({value, done}) => {
      if (done) {
        return chunks;
      }
      chunks.push(value);
      return pump();
    });
  }

  return pump();
};
const r = new Request();
export default r;