export default {
  syllabus_search: '/syllabus/my',
  fetch_node: '/syllabus/api/get',
  post_new_node: (ntype) => `/${ntype}/new`,
  update_node: (ntype) => `/${ntype}/update`,
};
