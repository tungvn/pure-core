import {reducer as formReducer} from "redux-form";
import {routerReducer} from "react-router-redux";
import User from "./user/Auth";
import CommonState from "./common";
import layout from "./layout";
import siteLanguage from './language';

export default {
  user: User,
  common: CommonState,
  layout,
  siteLanguage,
  form: formReducer,
  routing: routerReducer,
};

