/**
 * Created by Peter Hoang Nguyen on 3/17/2017.
 */
import {
  LOGIN_ACTION,
  LOGOUT_ACTION,
  ACTIVE_LOGIN_TAB_ACTION,
  LOGIN_SUCCESS_ACTION,
  OPEN_LOGIN_DIALOG,
  CLOSE_LOGIN_DIALOG,
  ACTIVE_REGISTER_TAB_ACTION,
  RESTORE_USER_INFO
} from "components/user/auth/actions";
import Configs from "configs/configuration";
import {
  ON_OAUTH2_LOGIN_SUCCESS_ACTION,
  ON_OAUTH2_GET_AGENCY_INFO_SUCCESS,
  ON_OAUTH2_RELOGIN_SUCCESS_ACTION
} from "components/user/auth/oauth2/actions";


const userInitialState = {
  isLoginTabActivated: true,
  openLoginDialog: false,
  info: {},
  oauth: {},
  agency: {},
};

const User = (state = userInitialState, action) => {
  let newState = {};
  switch (action.type) {

    case RESTORE_USER_INFO:
      const { user } = action;
      newState = {
        ...state,
        oauth: user.oauth,
        info: user.info,
        agency: user.agency,
      };
      break;

    case ON_OAUTH2_LOGIN_SUCCESS_ACTION:
      newState = {
        ...state,
        oauth: action.oauth,
      };
      break;
    case ON_OAUTH2_RELOGIN_SUCCESS_ACTION:
      const newOauth = { ...state.oauth };
      newOauth.timeExpriedAt = (Configs.defaultServerExpriedIn - 200) * 1000 + new Date().getTime(); // lấy độ trễ là 200s

      newState = {
        ...state,
        oauth: newOauth,
      };
      break;

    case ACTIVE_LOGIN_TAB_ACTION:
      newState = {
        ...state,
        isLoginTabActivated: true,
      };
      break;

    case LOGIN_ACTION:
      newState = {
        ...state,
        isLoginTabActivated: true,
      };
      break;

    case LOGIN_SUCCESS_ACTION:
      newState = {
        ...state,
        info: action.userInfo,
      };
      break;
    case ON_OAUTH2_GET_AGENCY_INFO_SUCCESS:
      newState = {
        ...state,
        agency: action.agency,
      };
      break;

    case LOGOUT_ACTION:

      newState = {
        ...state,
        isLoginTabActivated: true,
      };
      break;

    case ACTIVE_REGISTER_TAB_ACTION:
      newState = {
        ...state,
        isLoginTabActivated: false,
      };
      break;
    case OPEN_LOGIN_DIALOG:
      newState = {
        ...state,
        openLoginDialog: action.openLoginDialog,
      };
      break;
    case CLOSE_LOGIN_DIALOG:
      newState = {
        ...state,
        openLoginDialog: action.openLoginDialog,
      };
      break;
    default:
      return state;

  }
  return newState;
};
export default User;
