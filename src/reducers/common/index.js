/**
 * Created by Peter Hoang Nguyen on 3/17/2017.
 */
// import {QUICKVIEW_VISIBLE_STATUS, SHARE_COLLECTION} from "components/collection/quickview/actions";
// import {SET_IMAGE_DETAIL_MENU_STATE} from "components/collection/detail/actions";
import {STORE_SITE_CONFIG, REQUESTING_STATUS} from "layouts/actions";

const commonState = {
  showQuickView: false,
  showQuickViewTabNumber: 0,
  showingImageDetailMenu: false,
  showSearchOverlay: false,
  showSettingsAlert: false,
  showUpgradeAlert: false,
  showDownloadAlert: false,
  showDownloadingIcon: false,
  downloadMessage: '',
  Notify: {},
  requestings: {},
  shareCollectionId: undefined,
  scrollBody: {
    scrollPosition: 0,
  },
};

const CommonState = (state = commonState, action) => {
  let newState = {};
  switch (action.type) {
    case STORE_SITE_CONFIG:
      newState = {
        ...state,
        siteConfigs: action.siteConfigs,
      };
      break;

    case 'PAGING_KEYBOARD':
      newState = {
        ...state,
        activeKeyboardForPaging: action.isPagingKeyboard,
      };
      break;
    case REQUESTING_STATUS:
      const {id, status} = action;
      newState = {
        ...state,
        requestings: {...state.requestings, [id]: status},
      };
      break;
    default:
      return state;

  }
  return newState;
};
export default CommonState;
