const Configuration = {
  bodyScroll: 'bodyScroll',
  autoDetectMissingMessage: true,
  defaultServerExpriedIn: 3600, // mặc đinh server sẽ hết hạn trong bao lâu sau khi refresh token
  media: {
    img: {
      path: `${mediaRoot}images`,
    },
  },
  rejected_by_client_status: {
    id: 20,
    name: 'REJECTED BY CLIENT'
  },
  rejection_marked_status: {
    id: -20,
    name: 'MARKED IMAGE'
  },
  document_normal_status: {
    id: 10,
    name: 'Normal IMAGE'
  },
  defaultLogo: `${mediaRoot}images/default/lotus.png`,
  userLocalStorageKey: 'user',
  
  oauth2Configs: {
    client_id: 'd9ce362f-b6df-4ab8-9835-f261d1c110fd',
    client_secret: 'uW7///HxKn97nYLFqX9I6w==',
    grant_type: 'password',
    scope: 'api.read offline_access',
  },
  oauth2ConfigsResetToken: {
    client_id: 'd9ce362f-b6df-4ab8-9835-f261d1c110fd',
    client_secret: 'uW7///HxKn97nYLFqX9I6w==',
  },
  // oauth2Configs: {
  //   client_id: '11492dea-3fd5-4e8e-a175-3f5f11b81e04',
  //   client_secret: 'WWKQd6UzqeuW8zFQdc7MYA==',
  //   grant_type: 'client_credentials',
  //   scope: 'api.read',
  // },
  apiUrl: {
    // collections: 'http://localhost:3000/api/v1/collections.js',
    oauthToken:rememberMe => `/v1/user/login?remember=${rememberMe}`,
    oauthRevocationToken: '/v1/user/refresh_token',
    userInfo: '/v1/client',
    userLanguage: (languageId) => `v1/dictionary/photoportal/${languageId}`,
    siteConfigs: (site) => `${process.env.REACT_APP_SERVER_SITE_CONFIG_API_URL}/${site}/${process.env.REACT_APP_SERVER_SITE_CONFIG_EVN}`,
  },
  loginUrl: '/login',
};

const mediaRoot = '/media/';
export default Configuration;
