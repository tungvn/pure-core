import React from 'react';
import {IntlProvider, addLocaleData} from 'react-intl';
import vi from 'react-intl/locale-data/vi.js';
import en from 'react-intl/locale-data/en.js';
import uk from 'react-intl/locale-data/uk.js';
import ar from 'react-intl/locale-data/ar.js';
import nl from 'react-intl/locale-data/nl.js';
import no from 'react-intl/locale-data/no.js';
import se from 'react-intl/locale-data/se.js';
import es from 'react-intl/locale-data/es.js';
import de from 'react-intl/locale-data/de.js';
import da from 'react-intl/locale-data/da.js';
import Configs from 'configs/configuration';
import Requester from 'common/network/http/Request';
import DefaultLanguage from './DefaultLanugage';
import {connect} from 'react-redux';
import {getTimeEnableForCacheConfig} from 'common';

addLocaleData([...en, ...vi, ...uk, ...ar, ...nl, ...no, ...se, ...es, ...de, ...da]);

/**
 * Created by Peter Hoang Nguyen
 * Email: vntopmas@gmail.com
 * Tel: 0966298666
 * created date 23/06/2017
 **/
class I18nAsync extends React.Component {
  constructor() {
    super();
    this.state = {
      messages: {en: DefaultLanguage, language: {}},
    };
  }

  getLanguagePresent = () => {
    const {userInfo} = this.props;
    return userInfo['language'] || {language_id: 10, language_iso_code: 'en'};

  }

  componentWillReceiveProps(nextProps) {
    const {dispatch} = this.props;
    let {language} = this.state;
    let userLanguage = this.getLanguagePresent();
    language = language || {};
    if (language['language_id'] === userLanguage['language_id']) {
      return;
    }

    this.setState({language: userLanguage});
    setTimeout(() => {
      let messages = this.getMessagesAtClientSite(userLanguage['language_iso_code']);
      let cache = process.env.REACT_APP_LANGUAGE_CACHING;
      if(cache && cache.toLowerCase() === 'true') {
        if (!messages || !messages.timeToReloadData || (messages.timeToReloadData && messages.timeToReloadData < new Date().getTime())) {
          Requester.getJSON(Configs.apiUrl.userLanguage(userLanguage['language_id']), undefined, undefined,
            (response) => {
              const messagesLoaded = response.data || {};
              messagesLoaded.timeToReloadData = new Date().getTime() + getTimeEnableForCacheConfig();
              dispatch({type: 'SET_MESSAGES', locale: userLanguage['language_iso_code'], messages: messagesLoaded});
            });
        }
      } else {
        Requester.getJSON(Configs.apiUrl.userLanguage(userLanguage['language_id']), undefined, undefined,
          (response) => {
            const messagesLoaded = response.data || {};
            messagesLoaded.timeToReloadData = new Date().getTime() + getTimeEnableForCacheConfig();
            dispatch({type: 'SET_MESSAGES', locale: userLanguage['language_iso_code'], messages: messagesLoaded});
          });
      }

    }, 50);

  }

  getMessagesAtClientSite = (languageCode) => {
    const {userInfo, siteLanguage} = this.props;
    let messages = siteLanguage['messages'];
    if(!messages || (messages && Object.keys(messages).length === 0)) {
      let stLanguage = localStorage.getItem('reduxPersist:siteLanguage');
      if(stLanguage) {
        stLanguage = JSON.parse(stLanguage);
      }
      if(stLanguage) {
        messages = stLanguage['messages'];
      }
    }

    if (!messages || !messages[languageCode]) {
      return null;
    }
    return messages[languageCode];
  }

  render() {
    const {children, siteLanguage} = this.props;
    let {locale, messages} = siteLanguage;
    if(!messages[locale]) {
      messages = DefaultLanguage;
      locale = 'vi';
    }
    return (
      <IntlProvider key={locale} locale={locale} messages={messages[locale]}>
        {children}
      </IntlProvider>
    );
  }
}

const mapStateToProps = (state) => ({
  userInfo: state.user.info || {},
  siteLanguage: state.siteLanguage
});
export default connect(mapStateToProps)(I18nAsync);

